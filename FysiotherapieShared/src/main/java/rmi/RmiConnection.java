/**
 * 
 */
package rmi;

import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author robbie
 *
 */
public class RmiConnection {

	private Registry registry;

	public RmiConnection(String hostname) {

		String securityPolicyFile = "http://" + hostname
				+ "/classes/resources/security.policy";
		System.setProperty("java.security.policy", securityPolicyFile);

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

		try {
			registry = LocateRegistry.getRegistry(hostname);
		} catch (java.security.AccessControlException e) {
			System.out.println("Could not connect to registry: "
					+ e.getMessage());
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public Remote getService(String servicename) {
		try {
			return registry.lookup(servicename);
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		return null;
	}
}