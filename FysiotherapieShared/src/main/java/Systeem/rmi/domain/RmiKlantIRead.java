/**
 * 
 */
package Systeem.rmi.domain;

import java.util.Date;

import businessEntityDomain.ImmutablePatient;

/**
 * @author Gregor
 *
 */
public interface RmiKlantIRead extends ImmutablePatient {

	/**
	 * Getter voor geboortedatum.
	 *
	 * @return geboortedatum
	 */
	public Date getGeboortedatum();


}