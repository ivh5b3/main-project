package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Hack for providing a class to access the property file.
 *
 * @author Sander van Belleghem
 *
 */
public class Settings extends Properties {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1270039270790299294L;

	public Settings() {
		
		InputStream inputFile = null;

		try {
			inputFile = new FileInputStream("./resources/config.properties");
			
			if (inputFile != null) {
				load(inputFile);
			}
		} catch (IOException e) {
			System.out.println("Error reading file: " + e.getMessage() + " - aborting.");
			System.exit(0);
		}
	}
}
