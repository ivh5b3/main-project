/**
 * 
 */
package businessEntityDomain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * @author robbie
 *
 */
public class DatumFilter implements FilterInf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6064140675417315252L;
	private Timestamp beginDatum;
	private Timestamp eindDatum;
	private boolean totOneindig;
	private boolean overlap;

	public DatumFilter(Timestamp beginDatum, Timestamp eindDatum,
			boolean overlap, boolean totOneindig) {
		this.beginDatum = beginDatum;
		this.eindDatum = eindDatum;
		this.totOneindig = totOneindig;
		this.overlap = overlap;
	}

	public DatumFilter(Timestamp beginDatum, boolean totOneindig) {
		this(beginDatum, null, false, totOneindig);
	}

	public DatumFilter(Timestamp beginDatum) {
		this(beginDatum, false);
	}

	public DatumFilter(Timestamp beginDatum, Timestamp eindDatum) {
		this(beginDatum, eindDatum, false);
	}

	public DatumFilter(Timestamp beginDatum, Timestamp eindDatum,
			boolean overLap) {
		this(beginDatum, eindDatum, overLap, false);
	}

	public boolean checkConstraints(Object object) {

		if (object instanceof ImmutableSessie) {

			ImmutableSessie sessie = (ImmutableSessie) object;
			Timestamp sessieBeginDatum = sessie.getBeginDatumTijd();

			if (eindDatum == null) {
				if (totOneindig) {
					// Alleen de sessies van de beginDatum tot in de toekomst
					// mogen

					if (beginDatum.before(sessieBeginDatum)) {
						// de begin datum van de sessie is later dan de
						// opgegeven begin datum
						return true;
					}

					// de begin datum van de sessie ligt voor de opgegeven begin
					// datum
					return false;
				}

				// Er is alleen een begin datum opgegeven. Dit betekent dat
				// alleen sessies
				// die op die dag zijn door de filter moeten komen.
				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(beginDatum);

				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(sessieBeginDatum);

				return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
						&& cal1.get(Calendar.DAY_OF_YEAR) == cal2
								.get(Calendar.DAY_OF_YEAR);
			} else {
				// Begin en eind datum zijn beide ingevuld
				Timestamp sessieEindDatum = sessie.getEindDatumTijd();

				if (overlap) {
					// Ook sessies die overlappen met de begin of eind datum
					// moeten door de filter komen.
					// Bijvoorbeeld als de begin datum van de filter 11:00 is en
					// er is een sessie die
					// begint om 10:50 en eindigt om 11:10 moet deze ook door de
					// filter komen.
					// Een sessie die eindigt om 11:00 bij een ingestelde filter
					// met begin datum van 11:00
					// zal niet door de filter komen.

					if (eindDatum.after(sessieBeginDatum)
							&& beginDatum.before(sessieEindDatum)) {
						return true;
					}

					return false;
				}

				// Enkel begin en eind datum zijn ingevuld, dit betekent dat
				// alleen de sessies door de
				// filter komen die in zijn geheel tussen de begin en eind datum
				// van de filter vallen.
				if (beginDatum.before(sessieBeginDatum)
						&& eindDatum.after(sessieEindDatum)) {
					return true;
				}
			}

			return false;
		}

		// De functie geeft altijd true terug als het niet om een Sessie object
		// gaat
		return true;
	}
}
