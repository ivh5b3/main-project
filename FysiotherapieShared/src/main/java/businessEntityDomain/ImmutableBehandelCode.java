/**
 * 
 */
package businessEntityDomain;

/**
 * @author robbie
 *
 */
public interface ImmutableBehandelCode {
	public String getBehandelCode();
	
	public String getBehandelingNaam();
	
	public int getAantalSessies();
	
	public long getSessieDuur();
	
	public double getTariefBehandeling();
}
