/**
 * 
 */
package businessEntityDomain;

import java.io.Serializable;

/**
 * @author robbie
 *
 */
public class StatusFilter implements FilterInf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2374383583305354524L;
	private String status;

	public StatusFilter(String status) {
		this.status = status;
	}

	public boolean checkConstraints(Object object) {

		if (object instanceof ImmutableSessie) {

			ImmutableSessie sessie = (ImmutableSessie) object;

			if (!sessie.getStatus().equals(status)) {
				return false;
			}
		}

		// De functie geeft altijd true terug als het niet om een Sessie object
		// gaat
		return true;
	}
}