package businessEntityDomain;

import java.io.Serializable;

import datastorage.XMLStorable;

public class Fysiotherapeut extends Persoon implements ImmutableFysiotherapeut,
		XMLStorable, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5377492504938035693L;

	private String rekeningnummer;

	public Fysiotherapeut(String bsn, String voornaam, String achternaam,
			String geslacht, String adres, String postcode, String plaats,
			String telefoonnummer, String email, String rekeningnummer) {
		super(bsn, voornaam, achternaam, geslacht, adres, postcode, plaats,
				telefoonnummer, email);

		this.rekeningnummer = rekeningnummer;
	}

	public void update(String voornaam, String achternaam, String geslacht,
			String adres, String postcode, String plaats,
			String telefoonnummer, String email, String rekeningnummer) {
		this.voornaam = voornaam;
		this.achternaam = achternaam;
		this.geslacht = geslacht;
		this.adres = adres;
		this.postcode = postcode;
		this.plaats = plaats;
		this.telefoonnummer = telefoonnummer;
		this.email = email;
		this.rekeningnummer = rekeningnummer;
	}

	@Override
	public String getRekeningnummer() {
		return rekeningnummer;
	}
}
