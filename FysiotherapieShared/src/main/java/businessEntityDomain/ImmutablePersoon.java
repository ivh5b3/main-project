package businessEntityDomain;

public interface ImmutablePersoon {
	public String getBsn();
	
	public String getVoornaam();
	
	public String getAchternaam();
	
	public String getGeslacht();
	
	public String getAdres();
	
	public String getPostcode();
	
	public String getPlaats();
	
	public String getTelefoonnummer();
	
	public String getEmail();
}
