package businessEntityDomain;

import java.io.Serializable;

import datastorage.XMLStorable;

public class User implements ImmutableUser, XMLStorable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5195080112007670753L;
	private String username;
	private String password;
	private String userNumber;
	private String functie;
	
	public User(String username, String password, String functie) {
		this.username = username;
		this.password = password;
		this.functie = functie;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
	
	public String getUserNumber() {
		return userNumber;
	}
	
	public String getFunctie() {
		return functie;
	}
	
	public void setUserNumber(String number) {
		userNumber = number;
	}
	
	public void update (String username, String password) {
		this.username = username;
		this.password = password;
	}
}