package businessEntityDomain;

import java.io.Serializable;

public class Patient extends Persoon implements ImmutablePatient, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4387L;

	public Patient(String bsn, String voornaam, String achternaam,
			String geslacht, String adres, String postcode, String plaats,
			String telefoonnummer, String email) {
		super(bsn, voornaam, achternaam, geslacht, adres, postcode, plaats,
				telefoonnummer, email);
	}

}