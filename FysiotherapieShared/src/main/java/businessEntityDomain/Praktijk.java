package businessEntityDomain;

import java.io.Serializable;

import datastorage.XMLStorable;

public class Praktijk implements ImmutablePraktijk, XMLStorable, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1023849966836499268L;
	private String naam;
	private String adres;
	private String postcode;
	private String plaats;
	private String telefoonNummer;
	private String email;
	private int kvk;
	private String btwNummer;

	public Praktijk(String naam, String adres, String postcode, String plaats,
			String telefoonNummer, String email, int kvk, String btwNummer) {
		this.naam = naam;
		this.adres = adres;
		this.postcode = postcode;
		this.plaats = plaats;
		this.telefoonNummer = telefoonNummer;
		this.email = email;
		this.kvk = kvk;
		this.btwNummer = btwNummer;
	}

	public void update(String naam, String adres, String postcode,
			String plaats, String telefoonNummer, String email, int kvk,
			String btwNummer) {
		this.naam = naam;
		this.adres = adres;
		this.postcode = postcode;
		this.plaats = plaats;
		this.telefoonNummer = telefoonNummer;
		this.email = email;
		this.kvk = kvk;
		this.btwNummer = btwNummer;
	}

	public String getNaam() {
		return naam;
	}

	public String getAdres() {
		return adres;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getPlaats() {
		return plaats;
	}

	public String getTelefoonnummer() {
		return telefoonNummer;
	}

	public String getEmail() {
		return email;
	}

	public int getKvk() {
		return kvk;
	}

	public String getBtwNummer() {
		return btwNummer;
	}
}