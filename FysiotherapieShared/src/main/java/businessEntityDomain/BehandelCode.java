/**
 * 
 */
package businessEntityDomain;

import java.io.Serializable;

import datastorage.XMLStorable;

public class BehandelCode implements ImmutableBehandelCode, XMLStorable,
		Serializable {

	/**
	 * @author robbie
	 *
	 */
	private static final long serialVersionUID = -8054930537313388518L;
	private String behandelCode;
	private String behandelingNaam;
	private int aantalSessies;
	private double sessieDuur;
	private double tariefBehandeling;

	public BehandelCode(String behandelCode, String behandelingNaam,
			int aantalSessies, double sessieDuur, double tariefBehandeling) {

		this.behandelCode = behandelCode;
		this.behandelingNaam = behandelingNaam;
		this.aantalSessies = aantalSessies;
		this.sessieDuur = sessieDuur;
		this.tariefBehandeling = tariefBehandeling;
	}

	@Override
	public String getBehandelCode() {
		return behandelCode;
	}

	@Override
	public String getBehandelingNaam() {
		return behandelingNaam;
	}

	@Override
	public int getAantalSessies() {
		return aantalSessies;
	}

	@Override
	public long getSessieDuur() {
		double result = 0;
		int inHour = 60 * 60 * 1000;

		// Bereken eerst voor het aantal hele uren
		long sessieDuurHoleNumbers = (long) sessieDuur;

		result = sessieDuurHoleNumbers * inHour;

		// Vervolgens voor elke decimal

		// Haal de hele nummers af van decimal zodat we alleen de getallen
		// achter de komma overhouden
		double sessieDuurDecimals = sessieDuur - sessieDuurHoleNumbers;
		
		// Deel door ��n om de deel factor te krijgen
		double divideFactor = 1 / sessieDuurDecimals;

		result += inHour / divideFactor;

		return (long) result;
	}

	@Override
	public double getTariefBehandeling() {
		return tariefBehandeling;
	}
}
