package businessEntityDomain;

public interface ImmutableUser {

	public String getUsername();

	public String getPassword();

	public String getUserNumber();
}
