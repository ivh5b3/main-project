package businessEntityDomain;

import java.sql.Timestamp;

public interface ImmutableSessie {

	public Timestamp getBeginDatumTijd();

	public Timestamp getEindDatumTijd();

	public String getStatus();

	public int getSessieNummer();

	public String getOpmerking();

	public int getBehandelTrajectId();
}
