package businessEntityDomain;


/**
 * @author robbie
 *
 */
public interface ImmutableFysiotherapeut extends ImmutablePersoon  {

	public String getRekeningnummer();
}
