/**
 * 
 */
package businessEntityDomain;

/**
 * @author robbie
 *
 */
public interface FilterInf {

	public boolean checkConstraints(Object object);
}
