/**
 * 
 */
package businessEntityDomain;

import java.io.Serializable;

/**
 * @author robbie
 *
 */
public class PatientFilter implements FilterInf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5926635629608173358L;
	private String bsn;

	public PatientFilter(String bsn) {
		this.bsn = bsn;
	}

	public boolean checkConstraints(Object object) {

		if (object instanceof ImmutableBehandelTraject) {

			ImmutableBehandelTraject behandelTraject = (ImmutableBehandelTraject) object;

			if (!behandelTraject.getPatient().getBsn().equals(bsn)) {
				return false;
			}
		}

		// De functie geeft altijd true terug als het niet om een Sessie object
		// gaat
		return true;
	}
}