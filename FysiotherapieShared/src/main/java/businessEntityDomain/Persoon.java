package businessEntityDomain;

import java.io.Serializable;

public abstract class Persoon implements ImmutablePersoon, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6517228769702684188L;
	
	protected String bsn;
	protected String voornaam;
	protected String achternaam;
	protected String geslacht;
	protected String adres;
	protected String postcode;
	protected String plaats;
	protected String telefoonnummer;
	protected String email;
	
	protected Persoon (String bsn, String voornaam, String achternaam, String geslacht, String adres, String postcode, String plaats, String telefoonnummer, String email){
		this.bsn = bsn;
		this.voornaam = voornaam;
		this.achternaam = achternaam;
		this.geslacht = geslacht;
		this.adres = adres;
		this.postcode = postcode;
		this.plaats = plaats;
		this.telefoonnummer = telefoonnummer;
		this.email = email;
	}
	
	public String getBsn() {
		return bsn;
	}
	public String getVoornaam() {
		return voornaam;
	}
	
	public String getAchternaam() {
			
		return achternaam;
	}
	
	public String getGeslacht() {
		return geslacht;
	}
	
	public String getAdres() {
		return adres;
	}
	
	public String getPostcode() {
		return postcode;
	}
	
	public String getPlaats() {
		return plaats;
	}
	
	public String getTelefoonnummer() {
		return telefoonnummer;
	}
	
	public String getEmail() {
		return email;
	}
}