package businessEntityDomain;

public interface ImmutablePraktijk {
	public String getNaam();

	public String getAdres();

	public String getPostcode();

	public String getPlaats();

	public String getTelefoonnummer();

	public String getEmail();

	public int getKvk();

	public String getBtwNummer();
}