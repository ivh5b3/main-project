/**
 * 
 */
package businessEntityDomain;

import java.util.Date;

/**
 * @author Gregor
 *
 */
public interface RmiKlantIRead extends ImmutablePatient {

	/**
	 * Getter voor geboortedatum.
	 *
	 * @return geboortedatum
	 */
	public Date getGeboortedatum();


}