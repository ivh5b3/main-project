package businessEntityDomain;

import java.io.Serializable;
import java.util.ArrayList;

import datastorage.XMLStorable;

public class BehandelTraject implements ImmutableBehandelTraject, XMLStorable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2150146772220226231L;
	private BehandelCode behandelCode;
	private ArrayList<ImmutableSessie> sessies;
	private ImmutablePatient patient;
	private Fysiotherapeut fysiotherapeut;
	private int id;

	public BehandelTraject(ImmutablePatient patient, Fysiotherapeut fysiotherapeut,
			BehandelCode behandelCode) {
		this.patient = patient;
		this.fysiotherapeut = fysiotherapeut;
		this.behandelCode = behandelCode;
		this.sessies = new ArrayList<ImmutableSessie>();
	}

	public BehandelCode getBehandelCode() {
		return behandelCode;
	}

	public void addSessie(ImmutableSessie Sessie) {
		sessies.add(Sessie);
	}

	@Override
	public ImmutablePatient getPatient() {
		return patient;
	}

	@Override
	public ImmutableFysiotherapeut getFysiotherapeut() {
		return fysiotherapeut;
	}
	
	@Override
	public ArrayList<ImmutableSessie> getSessies() {
		return sessies;
	}

	@Override
	public ArrayList<ImmutableSessie> getSessies(FilterInf[] filters) {
		ArrayList<ImmutableSessie> tempSessies = new ArrayList<ImmutableSessie>();
		
		for (ImmutableSessie sessie : getSessies()) {
			boolean passed = true;

			for (FilterInf filter : filters) {
				if (!filter.checkConstraints(sessie)) {
					passed = false;

					break;
				}
			}

			if (passed) {
				tempSessies.add(sessie);
			}
		}
		
		return tempSessies;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int getId() {
		return id;
	}

}