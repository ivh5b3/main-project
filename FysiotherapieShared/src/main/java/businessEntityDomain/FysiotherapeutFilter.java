/**
 * 
 */
package businessEntityDomain;

import java.io.Serializable;

/**
 * @author robbie
 *
 */
public class FysiotherapeutFilter implements FilterInf, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7707743689092832638L;
	private ImmutableFysiotherapeut fysiotherapeut;

	public FysiotherapeutFilter(ImmutableFysiotherapeut fysiotherapeut) {
		this.fysiotherapeut = fysiotherapeut;
	}

	@Override
	public boolean checkConstraints(Object object) {

		if (object instanceof ImmutableBehandelTraject) {

			ImmutableBehandelTraject behandelTraject = (ImmutableBehandelTraject) object;

			if (!behandelTraject.getFysiotherapeut().getBsn()
					.equals(fysiotherapeut.getBsn())) {
				return false;
			}
		}

		// De functie geeft altijd true terug als het niet om een
		// BehandelTraject object
		// gaat
		return true;
	}

}
