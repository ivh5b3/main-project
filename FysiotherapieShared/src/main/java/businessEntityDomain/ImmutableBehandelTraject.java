package businessEntityDomain;

import java.util.ArrayList;

public interface ImmutableBehandelTraject {
	public BehandelCode getBehandelCode();

	public ImmutablePatient getPatient();

	public ImmutableFysiotherapeut getFysiotherapeut();

	public ArrayList<ImmutableSessie> getSessies();
	
	public ArrayList<ImmutableSessie> getSessies(FilterInf[] filters);

	public int getId();
}