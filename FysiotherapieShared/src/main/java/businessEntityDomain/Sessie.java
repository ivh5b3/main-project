package businessEntityDomain;

import java.io.Serializable;
import java.sql.Timestamp;

import datastorage.XMLStorable;

public class Sessie implements ImmutableSessie, XMLStorable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5195080112007670753L;
	private Timestamp beginDatumTijd;
	private Timestamp eindDatumTijd;
	private String status;
	private int sessieNummer;
	private String opmerking;
	private int behandelTrajectId;

	public Sessie(Timestamp beginDatumTijd, Timestamp eindDatumTijd,
			String status, int behandelTrajectId, String opmerking) {
		this.beginDatumTijd = beginDatumTijd;
		this.eindDatumTijd = eindDatumTijd;
		this.status = status;
		this.opmerking = opmerking;
		this.behandelTrajectId = behandelTrajectId;

	}

	public Timestamp getBeginDatumTijd() {
		return beginDatumTijd;
	}

	public Timestamp getEindDatumTijd() {
		return eindDatumTijd;
	}

	public String getStatus() {
		return status;
	}

	public int getSessieNummer() {
		return sessieNummer;
	}

	public String getOpmerking() {
		return opmerking;
	}

	public int getBehandelTrajectId() {
		return behandelTrajectId;
	}

	public void setSessieNummer(int nummer) {
		sessieNummer = nummer;
	}
	
	public void update (Timestamp beginDatum, Timestamp eindDatum,
				String status, String opmerking) {
		this.beginDatumTijd = beginDatum;
		this.eindDatumTijd = eindDatum;
		this.status = status;
		this.opmerking = opmerking;
	}
}