/**
 * 
 */
package businessEntityDomain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author robbie
 *
 */
public class DatumFilterTest {

	private static String status;
	private static int behandelTrajectId;
	private static String opmerking;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		status = "afgerond";
		behandelTrajectId = 2;
		opmerking = "opmerking";
	}

	@Test
	public void testOnlySessiesOnDate() {

		DatumFilter filter = new DatumFilter(
				Timestamp.valueOf("2014-11-02 14:00:00"));

		// Sessie die voor de gegeven timestamp is maar wel op dezelfde dag
		ImmutableSessie sessieBeforeTimestampButOnSameDay = new Sessie(
				Timestamp.valueOf("2014-11-02 00:00:00"),
				Timestamp.valueOf("2014-11-02 01:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie die op dezelfde dag is
		ImmutableSessie sessie = new Sessie(
				Timestamp.valueOf("2014-11-02 15:00:00"),
				Timestamp.valueOf("2014-11-02 16:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie die een dag later is
		ImmutableSessie sessieNotOnDay = new Sessie(
				Timestamp.valueOf("2014-11-03 00:00:00"),
				Timestamp.valueOf("2014-11-03 01:00:00"), status,
				behandelTrajectId, opmerking);

		assertTrue(filter.checkConstraints(sessieBeforeTimestampButOnSameDay));
		assertTrue(filter.checkConstraints(sessie));
		assertFalse(filter.checkConstraints(sessieNotOnDay));
	}

	@Test
	public void testLater() {

		DatumFilter filter = new DatumFilter(
				Timestamp.valueOf("2014-11-02 14:00:00"), true);

		// Sessie die net over de grens ligt en dus door de filter dient te
		// komen
		ImmutableSessie sessie = new Sessie(
				Timestamp.valueOf("2014-11-02 14:00:01"),
				Timestamp.valueOf("2014-11-02 16:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie die op een volgende dag is en dus ook door de filter dient te
		// komen
		ImmutableSessie sessieLater = new Sessie(
				Timestamp.valueOf("2014-12-02 14:00:00"),
				Timestamp.valueOf("2014-12-02 16:00:00"), status,
				behandelTrajectId, opmerking);

		// Deze sessie begint precies om 14:00 en moet dus niet door de filter
		// komen
		ImmutableSessie sessieBefore = new Sessie(
				Timestamp.valueOf("2014-11-02 14:00:00"),
				Timestamp.valueOf("2014-11-02 16:00:00"), status,
				behandelTrajectId, opmerking);

		assertTrue(filter.checkConstraints(sessie));
		assertTrue(filter.checkConstraints(sessieLater));
		assertFalse(filter.checkConstraints(sessieBefore));
	}

	@Test
	public void testBetweenTwoDatesWithoutOverlap() {

		DatumFilter filter = new DatumFilter(
				Timestamp.valueOf("2014-11-02 14:00:00"),
				Timestamp.valueOf("2014-11-05 14:00:00"));

		// Sessie die net over de grens begintijd grens ligt en stopt voor de
		// eindtijd grens, deze moet dus door de filter komen
		ImmutableSessie sessie = new Sessie(
				Timestamp.valueOf("2014-11-02 14:00:01"),
				Timestamp.valueOf("2014-11-02 16:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie die net over de grens van de eindtijd ligt en dus niet door de
		// filter dient te komen
		ImmutableSessie sessieLater = new Sessie(
				Timestamp.valueOf("2014-11-05 14:00:00"),
				Timestamp.valueOf("2014-11-05 16:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie die op de grens ligt als de begindatum en overlapping heeft,
		// moet niet door de filter komen
		ImmutableSessie sessieBefore = new Sessie(
				Timestamp.valueOf("2014-11-02 14:00:00"),
				Timestamp.valueOf("2014-11-02 16:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie die net voor de eindtijd grens ligt en in zijn geheel tussen
		// de begin- en eind tijd valt en dus door de filter dient te komen
		ImmutableSessie sessieBetween = new Sessie(
				Timestamp.valueOf("2014-11-05 13:00:00"),
				Timestamp.valueOf("2014-11-05 13:59:59"), status,
				behandelTrajectId, opmerking);

		// Sessie die net over de grens begintijd grens ligt en maar eindigt
		// op de eindtijd grens en dus niet getoont dient te worden
		ImmutableSessie sessieOverEndTime = new Sessie(
				Timestamp.valueOf("2014-11-02 14:00:01"),
				Timestamp.valueOf("2014-11-05 16:00:00"), status,
				behandelTrajectId, opmerking);

		assertTrue(filter.checkConstraints(sessie));
		assertFalse(filter.checkConstraints(sessieLater));
		assertFalse(filter.checkConstraints(sessieBefore));
		assertTrue(filter.checkConstraints(sessieBetween));
		assertFalse(filter.checkConstraints(sessieOverEndTime));
	}

	@Test
	public void testBetweenTwoDatesWithOverlap() {

		DatumFilter filter = new DatumFilter(
				Timestamp.valueOf("2014-11-02 14:00:00"),
				Timestamp.valueOf("2014-11-05 14:00:00"), true);

		// Sessie die tussen de begin- en eindtijd valt
		ImmutableSessie sessie = new Sessie(
				Timestamp.valueOf("2014-11-02 14:00:01"),
				Timestamp.valueOf("2014-11-02 16:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie die op de eindtijd grens begint en dus niet door de filter
		// dient te komen
		ImmutableSessie sessieLater = new Sessie(
				Timestamp.valueOf("2014-11-05 14:00:00"),
				Timestamp.valueOf("2014-11-05 16:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie waar de eindtijd op de begintijd grens ligt en dus niet door
		// de filter dient te komen
		ImmutableSessie sessieBefore = new Sessie(
				Timestamp.valueOf("2014-11-02 13:00:00"),
				Timestamp.valueOf("2014-11-02 14:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie waar begintijd op de begintijdgrens ligt en de eindtijd voor
		// de eindtijd grens ligt, deze moet dus door de filter komen
		ImmutableSessie sessieBeforeOverlap = new Sessie(
				Timestamp.valueOf("2014-11-02 13:59:00"),
				Timestamp.valueOf("2014-11-02 16:00:00"), status,
				behandelTrajectId, opmerking);

		// Sessie waar de gehele sessie tussen de begin- en eindtijd valt
		ImmutableSessie sessieBetween = new Sessie(
				Timestamp.valueOf("2014-11-05 13:00:00"),
				Timestamp.valueOf("2014-11-05 13:59:59"), status,
				behandelTrajectId, opmerking);

		// Sessie die begint voor de begintijdgrens en eindigt na de
		// eindtijdgrens, deze moet door de filter komen
		ImmutableSessie sessieStartsBeforeAndStopsAfter = new Sessie(
				Timestamp.valueOf("2014-11-05 13:00:00"),
				Timestamp.valueOf("2014-11-05 13:59:59"), status,
				behandelTrajectId, opmerking);

		assertTrue(filter.checkConstraints(sessie));
		assertFalse(filter.checkConstraints(sessieLater));
		assertFalse(filter.checkConstraints(sessieBefore));
		assertTrue(filter.checkConstraints(sessieBeforeOverlap));
		assertTrue(filter.checkConstraints(sessieBetween));
		assertTrue(filter.checkConstraints(sessieStartsBeforeAndStopsAfter));
	}
}
