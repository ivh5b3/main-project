/**
 * 
 */
package businessEntityDomain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author robbie
 *
 */
public class BehandelCodeTest {

	@Test
	public void getSessieDuurDecimalHighTest() {
		
		BehandelCode obj1 = new BehandelCode("001", "Hamstring", 5, 0.9, 10.00);
		
		// 0.9 zou het resultaat 3240000 moeten geven
		long expected1 = 3240000;
			
		assertEquals(expected1, obj1.getSessieDuur());
	}
	
	@Test
	public void getSessieDuurOneTest() {
		BehandelCode obj2 = new BehandelCode("001", "Hamstring", 5, 1, 10.00);
		
		// 1 zou het resultaat 3600000 moeten geven
		long expected2 = 3600000;
			
		assertEquals(expected2, obj2.getSessieDuur());
	}	
	
	@Test
	public void getSessieDuurOneAndDecimalTest() {
		BehandelCode obj3 = new BehandelCode("001", "Hamstring", 5, 1.1, 10.00);
		
		// 1.1 zou het resultaat 3960000 moeten geven
		long expected3 = 3960000;
			
		assertEquals(expected3, obj3.getSessieDuur());
	}	
	
	@Test
	public void getSessieDuurDecimalLowTest() {
		BehandelCode obj4 = new BehandelCode("001", "Hamstring", 5, 0.1, 10.00);
		
		// 0.1 zou het resultaat 360000 moeten geven
		long expected4 = 360000;
			
		assertEquals(expected4, obj4.getSessieDuur());
	}	
	
}
