package fysiotherapeut.presentation;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import businessEntityDomain.ImmutableFysiotherapeut;
import fysiotherapeut.businessLogic.FysiotherapeutManagerImpl;

public class FysiotherapeutUi {

	private FysiotherapeutManagerImpl manager;
	private JTable table;
	private JScrollPane scrollPane;
	private ArrayList<String> rowBSN;
	private JTextField naam;
	private JTextField adres;
	private JTextField postcode;
	private JTextField plaats;
	private JTextField bsn;
	private JTextField email;
	private JTextField telefoonNummer;
	private JTextField rekeningNummer;
	private JTextField achternaam;
	private ImmutableFysiotherapeut selectedFysiotherapeut;
	private JRadioButton rdbtnVrouw;
	private JRadioButton rdbtnMan;
	private JButton btnToevoegen;
	private JButton btnAnnuleren;
	private JButton btnWijzigen;
	private JPanel cardPanel;
	private CardLayout cardLayout;
	private JPanel popupPanelInvoeren;
	private JPanel popupPanelWijzigen;
	private JPanel defaultPanel;

	public FysiotherapeutUi(FysiotherapeutManagerImpl manager,
			CardLayout cardlayout, JPanel cardpanel) {
		this.manager = manager;

		cardLayout = cardlayout;
		cardPanel = cardpanel;
	}

	public JPanel getStartPanel() {
		JPanel panel = new JPanel();

		table = new JTable(getTableModel());

		table.setRowSelectionAllowed(true);
		table.setRowHeight(25);
		table.getTableHeader().setReorderingAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		scrollPane = new JScrollPane(table);
		scrollPane.revalidate();
		scrollPane.setViewportView(table);

		panel.setBounds(100, 100, 800, 600);

		JButton btnTerug = new JButton("< Terug");
		btnTerug.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cardLayout.show(cardPanel, "leiderGui");
			}
		});

		JButton btnToevoegen = new JButton("Toevoegen");
		btnToevoegen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				cardPanel.add(getPopupInvoerenPanel(), "Fysiotherapeut invoeren");
				cardLayout.show(cardPanel, "Fysiotherapeut invoeren");
			}
		});

		JButton btnWijzigen = new JButton("Wijzigen");
		btnWijzigen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null,
							"Er is nog geen fysiotherapeut geselecteerd!",
							"Fout: Wijzigen fysiotherapeut",
							JOptionPane.INFORMATION_MESSAGE);
							table.setModel(getTableModel());
				} else {
					selectedFysiotherapeut = manager.getFysiotherapeut(rowBSN.get(table.getSelectedRow()));
					cardPanel.add(getPopupWijzigenPanel(selectedFysiotherapeut), "Fysiotherapeut wijzigen");
					cardLayout.show(cardPanel, "Fysiotherapeut wijzigen");
				}
			}
		});

		JPanel panel2 = new JPanel();

		JButton btnVerwijder = new JButton("Verwijder");
		btnVerwijder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null,
							"Er is nog geen fysiotherapeut geselecteerd!",
							"Fout: Verwijderen fysiotherapeut",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					int dialogResult = JOptionPane
							.showConfirmDialog(
									null,
									"Weet u zeker dat u deze fysiotherapeut wilt verwijderen?",
									"Waarschuwing", JOptionPane.YES_NO_OPTION);
					if (dialogResult == JOptionPane.YES_OPTION) {
						selectedFysiotherapeut = manager
								.getFysiotherapeut(rowBSN.get(table
										.getSelectedRow()));
						System.out.println(selectedFysiotherapeut.getVoornaam());
						manager.deleteFysiotherapeut(selectedFysiotherapeut
								.getBsn());

						table.setModel(getTableModel());
						table.revalidate();
						table.repaint();
					}
				}
			}
		});

		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(6)
																		.addComponent(
																				panel2,
																				GroupLayout.DEFAULT_SIZE,
																				788,
																				Short.MAX_VALUE))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				btnTerug)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				btnToevoegen)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
//																		.addComponent(
//																				btnWijzigen)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnVerwijder)
																		.addContainerGap()))));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										groupLayout
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(btnTerug)
												.addComponent(btnToevoegen)
//												.addComponent(btnWijzigen)
												.addComponent(btnVerwijder))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(panel2,
										GroupLayout.PREFERRED_SIZE, 522,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(15, Short.MAX_VALUE)));
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));
		panel2.add(scrollPane);
		panel.setLayout(groupLayout);

		return panel;
	}

	public DefaultTableModel getTableModel() {

		Object columnNames[] = { "Naam", "Adres", "Postcode", "PLaats", "BSN",
				"Telefoonnummer", "E-mail", "Rekeningnummer", "Geslacht" };

		DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -8919261227066895178L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		tableModel.setRowCount(0);

		ArrayList<? extends ImmutableFysiotherapeut> fysiotherapeuten = manager
				.getFysiotherapeuten();
		rowBSN = new ArrayList<String>();

		for (ImmutableFysiotherapeut fysiotherapeut : fysiotherapeuten) {
			String naam = fysiotherapeut.getVoornaam() + " "
					+ fysiotherapeut.getAchternaam();
			String adres = fysiotherapeut.getAdres();
			String postcode = fysiotherapeut.getPostcode();
			String plaats = fysiotherapeut.getPlaats();
			String bsn = fysiotherapeut.getBsn();
			String telefoonnummer = fysiotherapeut.getTelefoonnummer();
			String email = fysiotherapeut.getEmail();
			String rekeningnummer = fysiotherapeut.getRekeningnummer();
			String geslacht = fysiotherapeut.getGeslacht();

			Object[] rowData = { naam, adres, postcode, plaats, bsn,
					telefoonnummer, email, rekeningnummer, geslacht };

			tableModel.addRow(rowData);

			rowBSN.add(bsn);
		}

		return tableModel;
	}

	public JPanel getPopupInvoerenPanel() {

		defaultPanel = new JPanel();
		
		popupPanelInvoeren = new JPanel(new GridLayout(3, 2));

		popupPanelInvoeren.setPreferredSize(new Dimension(300, 900));
		popupPanelInvoeren.setMaximumSize(popupPanelInvoeren.getPreferredSize());
		popupPanelInvoeren.setBorder(BorderFactory.createEmptyBorder(15, 25, 15, 25));
		
		JLabel lblFysiotherapeut = new JLabel("Voornaam");

		JLabel lblFa = new JLabel("Adres");

		JLabel lblPostcode = new JLabel("Postcode");

		JLabel lblPlaats = new JLabel("Plaats");

		JLabel lblTelefoonnummer = new JLabel("BSN");

		JLabel lblEmail = new JLabel("E-mail");

		JLabel lblKvkNummer = new JLabel("Telefoonnummer");

		JLabel lblBtwNummer = new JLabel("Rekeningnummer");

		JLabel lblAchternaam = new JLabel("Achternaam");

		JLabel lblGeslacht = new JLabel("Geslacht");

		achternaam = new JTextField();
		achternaam.setColumns(10);

		naam = new JTextField();
		naam.setColumns(10);

		adres = new JTextField();
		adres.setColumns(10);

		postcode = new JTextField();
		postcode.setColumns(10);

		plaats = new JTextField();
		plaats.setColumns(10);

		bsn = new JTextField();
		bsn.setColumns(10);

		email = new JTextField();
		email.setColumns(10);

		telefoonNummer = new JTextField();
		telefoonNummer.setColumns(10);

		rekeningNummer = new JTextField();
		rekeningNummer.setColumns(10);

		ButtonGroup group = new ButtonGroup();
		rdbtnVrouw = new JRadioButton("Vrouw");
		rdbtnMan = new JRadioButton("man");
		group.add(rdbtnVrouw);
		group.add(rdbtnMan);
		
		btnToevoegen = new JButton("invoeren");
		btnAnnuleren = new JButton("annuleren");

		btnToevoegen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ArrayList<String> fouten = voegFysiotherapeutToe();
				if (!fouten.isEmpty()) {
					String foutmeldingen = "";
					for (int i = 0; i < fouten.size(); i++) {
						foutmeldingen = foutmeldingen
								+ fouten.get(i)
								+ " \n";
					}

					JOptionPane.showMessageDialog(null,
						    foutmeldingen,
						    "Het volgende ging fout: ",
						    JOptionPane.PLAIN_MESSAGE);
				} else {
					cardLayout.show(cardPanel, "fysiotherapeutGui");
					table.setModel(getTableModel());
				}
				
			}
		});
		btnAnnuleren.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cardLayout.show(cardPanel, "fysiotherapeutGui");
			}
		});
		
		popupPanelInvoeren.add(btnAnnuleren);
		popupPanelInvoeren.add(btnToevoegen);
		
		GroupLayout groupLayout = new GroupLayout(popupPanelInvoeren);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																Alignment.LEADING,
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				lblKvkNummer)
																		.addGap(18)
																		.addComponent(
																				telefoonNummer,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblTelefoonnummer)
																						.addComponent(
																								lblPlaats)
																						.addComponent(
																								lblPostcode)
																						.addComponent(
																								lblBtwNummer)
																						.addComponent(
																								lblEmail)
																						.addComponent(
																								lblFysiotherapeut)
																						.addComponent(
																								lblAchternaam)
																						.addComponent(
																								lblFa)
																						.addComponent(
																								lblGeslacht)
																						.addComponent(btnAnnuleren))
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING,
																								false)
																						.addGroup(
																								groupLayout
																										.createSequentialGroup()
																										.addGap(15)
																										.addGroup(
																												groupLayout
																														.createParallelGroup(
																																Alignment.LEADING)
																														.addComponent(
																																email,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addGroup(
																																groupLayout
																																		.createSequentialGroup()
																																		.addComponent(
																																				rdbtnVrouw)
																																		.addGap(18)
																																		.addComponent(
																																				rdbtnMan))
																														.addGroup(
																																groupLayout
																																		.createSequentialGroup()
																																		.addComponent(btnToevoegen))
																														.addComponent(
																																adres,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																achternaam,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																naam,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																plaats,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																bsn,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																rekeningNummer,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																postcode,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE))))))
										.addGap(264)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblFysiotherapeut)
														.addComponent(
																naam,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblAchternaam)
														.addComponent(
																achternaam,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblFa)
														.addComponent(
																adres,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblPostcode)
														.addComponent(
																postcode,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblPlaats)
														.addComponent(
																plaats,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblTelefoonnummer)
														.addComponent(
																bsn,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblKvkNummer)
														.addComponent(
																telefoonNummer,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblEmail)
														.addComponent(
																email,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblBtwNummer)
														.addComponent(
																rekeningNummer,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																lblGeslacht)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				rdbtnVrouw)
																		.addComponent(
																				rdbtnMan)))
										.addGap(31)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(btnAnnuleren)
														.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(btnToevoegen)))
										.addContainerGap(60, Short.MAX_VALUE)));
		popupPanelInvoeren.setLayout(groupLayout);

		defaultPanel.add(popupPanelInvoeren);
		return defaultPanel;
	}

	public ArrayList<String> voegFysiotherapeutToe() {
		String tNaam = naam.getText();
		String tAchternaam = achternaam.getText();
		String tAdres = adres.getText();
		String tPostcode = postcode.getText();
		String tPlaats = plaats.getText();
		String tBsn = bsn.getText();
		String tTelefoonnummer = telefoonNummer.getText();
		String tEmail = email.getText();
		String tRekeningnummer = rekeningNummer.getText();
		String tGeslacht = "onbekend";
		ArrayList<String> fouten = new ArrayList<String>();

		if (!tNaam.matches("[a-zA-Z]+") || tNaam.isEmpty()
				|| tNaam.length() > 100) {
			String foutmelding = "De voornaam is niet correct ingevuld." + tNaam;
			fouten.add(foutmelding);
		}

		if (!tAchternaam.matches("[a-zA-Z]+") || tAchternaam.isEmpty()
				|| tAchternaam.length() > 100) {
			String foutmelding = "De achternaam is niet correct ingevuld." + tAchternaam;
			fouten.add(foutmelding);
		}

		if (tAdres == "" || tAdres.length() > 100) {
			String foutmelding = "Het adres is niet correct ingevuld." + tAdres;
			fouten.add(foutmelding);
		}

		if (!tPostcode.matches("[a-zA-Z0-9\\s]+") || tPostcode.isEmpty()
				|| tNaam.length() > 100) {
			String foutmelding = "De postcode is niet correct ingevuld." + tPostcode;
			fouten.add(foutmelding);
		}

		if (!tPlaats.matches("[a-zA-Z]+") || tPlaats.isEmpty()
				|| tPlaats.length() > 100) {
			String foutmelding = "De plaats is niet correct ingevuld." + tPlaats;
			fouten.add(foutmelding);
		}

		if (!tBsn.matches("[a-zA-Z0-9]+") || tBsn.isEmpty()
				|| tBsn.length() > 100) {
			String foutmelding = "Het bsn nummer is niet correct ingevuld." + tBsn;
			fouten.add(foutmelding);
		}

		if (tTelefoonnummer.matches("[a-zA-Z]+") || tTelefoonnummer.isEmpty()
				|| tTelefoonnummer.length() > 100) {
			String foutmelding = "Het telefoonnummer is niet correct ingevuld." + tTelefoonnummer;
			fouten.add(foutmelding);
		}

		if (tEmail.isEmpty() || tEmail.length() > 100) {
			String foutmelding = "De email is niet correct ingevuld." + tEmail;
			fouten.add(foutmelding);
		}

		if (!tRekeningnummer.matches("[a-zA-Z0-9]+")
				|| tRekeningnummer.isEmpty() || tRekeningnummer.length() > 100) {
			String foutmelding = "Het rekeningnummer is niet correct ingevuld." + tRekeningnummer;
			fouten.add(foutmelding);
		}
		if (rdbtnMan.isSelected() && rdbtnVrouw.isSelected()) {
			String foutmelding = "Als geslacht is zowel man als vrouw geselecteerd.";
			fouten.add(foutmelding);
		} else if (rdbtnMan.isSelected()) {
			tGeslacht = "Man";
		} else if (rdbtnVrouw.isSelected()) {
			tGeslacht = "Vrouw";
		} else {
			String foutmelding = "Er is geen geslacht geselecteerd.";
			fouten.add(foutmelding);
		}

		if (fouten.size() == 0) {

			manager.insertFysiotherapeut(tBsn, tNaam, tAchternaam, tGeslacht,
					tAdres, tPostcode, tPlaats, tTelefoonnummer, tEmail,
					tRekeningnummer);

		}
		
		return fouten;
	}

	public ArrayList<String> wijzigFysiotherapeut(String tBsn) {
		String tNaam = naam.getText();
		String tAchternaam = achternaam.getText();
		String tAdres = adres.getText();
		String tPostcode = postcode.getText();
		String tPlaats = plaats.getText();
		String tTelefoonnummer = telefoonNummer.getText();
		String tEmail = email.getText();
		String tRekeningnummer = rekeningNummer.getText();
		String tGeslacht = "onbekend";

		ArrayList<String> fouten = new ArrayList<String>();

		if (!tNaam.matches("[a-zA-Z]") || tNaam.isEmpty()
				|| tNaam.length() > 100) {
			String foutmelding = "De voornaam is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (!tAchternaam.matches("[a-zA-Z]") || tAchternaam.isEmpty()
				|| tAchternaam.length() > 100) {
			String foutmelding = "De achternaam is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (tAdres.isEmpty() || tAdres.length() > 100) {
			String foutmelding = "Het adres is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (!tPostcode.matches("[a-zA-Z0-9]") || tPostcode.isEmpty()
				|| tNaam.length() > 100) {
			String foutmelding = "De postcode is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (!tPlaats.matches("[a-zA-Z]s") || tPlaats.isEmpty()
				|| tPlaats.length() > 100) {
			String foutmelding = "De plaats is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (!tBsn.matches("[a-zA-Z0-9]") || tBsn.isEmpty()
				|| tBsn.length() > 100) {
			String foutmelding = "Het bsn nummer is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (tTelefoonnummer.matches("[a-zA-Z]") || tTelefoonnummer.isEmpty()
				|| tTelefoonnummer.length() > 100) {
			String foutmelding = "Het telefoonnummer is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (tEmail.isEmpty() || tEmail.length() > 100) {
			String foutmelding = "De email is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (!tRekeningnummer.matches("[a-zA-Z0-9]s")
				|| tRekeningnummer.isEmpty() || tRekeningnummer.length() > 100) {
			String foutmelding = "Het rekeningnummer is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (rdbtnMan.isSelected()) {
			tGeslacht = "Man";
		} else if (rdbtnVrouw.isSelected()) {
			tGeslacht = "Vrouw";
		} else {
			String foutmelding = "Er is geen geslacht geselecteerd.";
			fouten.add(foutmelding);
		}

		if (fouten.size() == 0) {

			manager.updateFysiotherapeut(tBsn, tNaam, tAchternaam, tGeslacht,
					tAdres, tPostcode, tPlaats, tTelefoonnummer, tEmail,
					tRekeningnummer);
		}
		
		return fouten;
	}

	public JPanel getPopupWijzigenPanel(
		ImmutableFysiotherapeut fysiotherapeutWijzigenObject) {

		defaultPanel = new JPanel();
		popupPanelWijzigen = new JPanel(new GridLayout(3, 2));

		popupPanelWijzigen.setPreferredSize(new Dimension(300, 900));
		popupPanelWijzigen.setMaximumSize(popupPanelWijzigen.getPreferredSize());
		popupPanelWijzigen.setBorder(BorderFactory.createEmptyBorder(15, 25, 15, 25));

		JLabel lblFysiotherapeut = new JLabel("Voornaam");

		JLabel lblFa = new JLabel("Adres");

		JLabel lblPostcode = new JLabel("Postcode");

		JLabel lblPlaats = new JLabel("Plaats");

		JLabel lblEmail = new JLabel("E-mail");

		JLabel lblKvkNummer = new JLabel("Telefoonnummer");

		JLabel lblBtwNummer = new JLabel("Rekeningnummer");

		JLabel lblAchternaam = new JLabel("Achternaam");

		JLabel lblGeslacht = new JLabel("Geslacht");

		achternaam = new JTextField(
				fysiotherapeutWijzigenObject.getAchternaam());
		achternaam.setColumns(10);

		naam = new JTextField(fysiotherapeutWijzigenObject.getVoornaam());
		naam.setColumns(10);

		adres = new JTextField(fysiotherapeutWijzigenObject.getAdres());
		adres.setColumns(10);

		postcode = new JTextField(fysiotherapeutWijzigenObject.getPostcode());
		postcode.setColumns(10);

		plaats = new JTextField(fysiotherapeutWijzigenObject.getPlaats());
		plaats.setColumns(10);

		email = new JTextField(fysiotherapeutWijzigenObject.getEmail());
		email.setColumns(10);

		telefoonNummer = new JTextField(
				fysiotherapeutWijzigenObject.getTelefoonnummer());
		telefoonNummer.setColumns(10);

		rekeningNummer = new JTextField(
				fysiotherapeutWijzigenObject.getRekeningnummer());
		rekeningNummer.setColumns(10);


		ButtonGroup group = new ButtonGroup();
		rdbtnVrouw = new JRadioButton("Vrouw");
		rdbtnMan = new JRadioButton("man");
		if (fysiotherapeutWijzigenObject.getGeslacht().equals("man")) {
			rdbtnMan.setSelected(true);
		} else {
			rdbtnVrouw.setSelected(true);
		}
		group.add(rdbtnVrouw);
		group.add(rdbtnMan);

		btnWijzigen = new JButton("wijzigen");

		btnWijzigen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ArrayList<String> fouten = wijzigFysiotherapeut(selectedFysiotherapeut.getBsn());
				if (!fouten.isEmpty()) {
					String foutmeldingen = "";
					for (int i = 0; i < fouten.size(); i++) {
						foutmeldingen = foutmeldingen
								+ fouten.get(i)
								+ " \n";
					}

					JOptionPane.showMessageDialog(null,
						    foutmeldingen,
						    "Het volgende ging fout: ",
						    JOptionPane.PLAIN_MESSAGE);
				} else {
					cardLayout.show(cardPanel, "fysiotherapeutGui");
					table.setModel(getTableModel());
				}
				
			}
		});
		
		popupPanelWijzigen.add(btnAnnuleren);
		popupPanelWijzigen.add(btnWijzigen);
		
		GroupLayout groupLayout = new GroupLayout(popupPanelWijzigen);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.LEADING)
																		.addGroup(
																				groupLayout
																						.createSequentialGroup()
																						.addComponent(
																								lblKvkNummer)
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addComponent(
																								telefoonNummer,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE))
																		.addGroup(
																				Alignment.TRAILING,
																				groupLayout
																						.createSequentialGroup()
																						.addGroup(
																								groupLayout
																										.createParallelGroup(
																												Alignment.LEADING)
																										.addComponent(
																												lblPlaats)
																										.addComponent(
																												lblPostcode)
																										.addComponent(
																												lblBtwNummer)
																										.addComponent(
																												lblEmail)
																										.addComponent(
																												lblFysiotherapeut)
																										.addComponent(
																												lblAchternaam)
																										.addComponent(
																												lblFa)
																										.addComponent(
																												lblGeslacht)
																										.addComponent(btnAnnuleren))
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addGroup(
																								groupLayout
																										.createParallelGroup(
																												Alignment.LEADING)
																										.addGroup(
																												groupLayout
																														.createSequentialGroup()
																														.addComponent(
																																rdbtnVrouw)
																														.addGap(18)
																														.addComponent(
																																rdbtnMan))
																										.addGroup(
																												groupLayout
																														.createSequentialGroup()
																														.addComponent(btnWijzigen))
																										.addComponent(
																												adres,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												achternaam,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												naam,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												email,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												plaats,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												rekeningNummer,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												postcode,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												btnWijzigen,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)))))
										.addContainerGap(303, Short.MAX_VALUE)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGap(30)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblFysiotherapeut)
														.addComponent(
																naam,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblAchternaam)
														.addComponent(
																achternaam,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblFa)
														.addComponent(
																adres,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblPostcode)
														.addComponent(
																postcode,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblPlaats)
														.addComponent(
																plaats,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblKvkNummer)
														.addComponent(
																telefoonNummer,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblEmail)
														.addComponent(
																email,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblBtwNummer)
														.addComponent(
																rekeningNummer,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																lblGeslacht)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				rdbtnVrouw)
																		.addComponent(
																				rdbtnMan)))
										.addGap(18)
										.addGroup(
												groupLayout
												.createParallelGroup(
														Alignment.LEADING)
												.addComponent(btnAnnuleren)
												.addGroup(
														groupLayout
																.createParallelGroup(
																		Alignment.BASELINE)
																.addComponent(
																		btnWijzigen)))
										.addContainerGap(109, Short.MAX_VALUE)));
		popupPanelWijzigen.setLayout(groupLayout);
		defaultPanel.add(popupPanelWijzigen);
		return defaultPanel;
	}
}