package fysiotherapeut.businessLogic;

import interfaces.BehandelTrajectDAOInf;
import interfaces.FysiotherapeutDAOInf;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import util.Settings;
import businessEntityDomain.BehandelTraject;
import businessEntityDomain.DatumFilter;
import businessEntityDomain.FilterInf;
import businessEntityDomain.Fysiotherapeut;
import businessEntityDomain.FysiotherapeutFilter;
import businessEntityDomain.ImmutableFysiotherapeut;
import datastorage.DAOFactory;

public class FysiotherapeutManagerImpl implements FysiotherapeutManager {

	private DAOFactory dao;
	private FysiotherapeutDAOInf fysioDAO;

	public FysiotherapeutManagerImpl() {
		try {

			Properties properties = new Settings();

			dao = DAOFactory
					.getDAOFactory(properties.getProperty("daofactory"));
			fysioDAO = dao.getFysiotherapeutDAO();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	private Fysiotherapeut getFysiotherapeutFromDAO(String bsn) {
		try {
			return fysioDAO.get(bsn);
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());
			
			return null;
		}
	}

	public ImmutableFysiotherapeut getFysiotherapeut(String bsn) {
		return getFysiotherapeutFromDAO(bsn);
	}

	public boolean insertFysiotherapeut(String bsn, String voornaam,
			String achternaam, String geslacht, String adres, String postcode,
			String plaats, String telefoonnummer, String email,
			String rekeningnummer) {

		try {
			return fysioDAO.insert(new Fysiotherapeut(bsn, voornaam,
					achternaam, geslacht, adres, postcode, plaats,
					telefoonnummer, email, rekeningnummer));
		} catch (RemoteException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	public boolean updateFysiotherapeut(String bsn, String voornaam,
			String achternaam, String geslacht, String adres, String postcode,
			String plaats, String telefoonnummer, String email,
			String rekeningnummer) {

		try {
			Fysiotherapeut fysio = getFysiotherapeutFromDAO(bsn);

			fysio.update(voornaam, achternaam, geslacht, adres, postcode,
					plaats, telefoonnummer, email, rekeningnummer);

			return fysioDAO.update(fysio);
		} catch (RemoteException e) {
			return false;
		}
	}

	public ArrayList<? extends ImmutableFysiotherapeut> getFysiotherapeuten() {
		try {
			return fysioDAO.getAll();
		} catch (RemoteException e) {
			return null;
		}
	}

	public boolean deleteFysiotherapeut(String bsn) {
		try {
			BehandelTrajectDAOInf behandelTrajectDAO = dao.getBehandelTrajectDAO();
			
			FilterInf[] filters = new FilterInf[2];
			
			filters[0] = new FysiotherapeutFilter(getFysiotherapeut(bsn));
			
			Date date = new Date();
			Timestamp beginDatum = new Timestamp(date.getTime());
			
			filters[1] = new DatumFilter(beginDatum, true);
			
			ArrayList<BehandelTraject> behandelTrajecten = behandelTrajectDAO.getBehandelingen(filters);
			
			boolean empty = true;
			
			if (!behandelTrajecten.isEmpty()) {
				for (BehandelTraject traject : behandelTrajecten) {
					if (!traject.getSessies(filters).isEmpty()) {
						empty = false;
						
						break;
					}
				}
				
			}
			
			if (empty) {
				return fysioDAO.delete(getFysiotherapeutFromDAO(bsn));
			}
			
			// Heeft nog open behandelingen
			return false;
		} catch (RemoteException e) {
			return false;
		}
	}
}