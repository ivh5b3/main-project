package fysiotherapeut.businessLogic;

import java.util.ArrayList;

import businessEntityDomain.ImmutableFysiotherapeut;

public interface FysiotherapeutManager {
	public ImmutableFysiotherapeut getFysiotherapeut(String bsn);

	public boolean insertFysiotherapeut(String bsn, String voornaam,
			String achternaam, String geslacht, String adres, String postcode,
			String plaats, String telefoonnummer, String email,
			String rekeningnummer);

	public boolean updateFysiotherapeut(String bsn, String voornaam,
			String achternaam, String geslacht, String adres, String postcode,
			String plaats, String telefoonnummer, String email,
			String rekeningnummer);

	public ArrayList<? extends ImmutableFysiotherapeut> getFysiotherapeuten();

	public boolean deleteFysiotherapeut(String bsn);
}