/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userSystem.businessLogic;

import interfaces.UserDAOInf;

import java.rmi.RemoteException;
import java.util.Properties;

import util.Settings;
import businessEntityDomain.User;
import datastorage.DAOFactory;

/**
 *
 * @author Sander van Belleghem
 */
public class UserManagerImpl implements UserManager {

	private DAOFactory daoFactory;
	private UserDAOInf userDAO;
	private String username;

	public UserManagerImpl() {

		try {
			Properties properties = new Settings();

			this.daoFactory = DAOFactory.getDAOFactory(properties
					.getProperty("daofactory"));
			this.userDAO = daoFactory.getUserDAO();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	@Override
	public boolean login(String username, String password) {
		try {
			this.username = username;
			
			return userDAO.login(username, password);

		} catch (RemoteException e) {
			System.out.println("Remote exception: " + e.getMessage());

			return false;
		}
	}

	@Override
	public User getUser(String username, String password) {
		try {
			return userDAO.getUser(username, password);

		} catch (RemoteException e) {
			System.out.println("Remote exception: " + e.getMessage());
		}
		
		return null;
	}

	
	@Override
	public boolean isLeidinggevende() {
		try {
			return userDAO.isLeidinggevende(username);
		} catch (RemoteException e) {
			System.out.println("Remote exception: " + e.getMessage());
			System.exit(0);
		}
		return false;
	}

}
