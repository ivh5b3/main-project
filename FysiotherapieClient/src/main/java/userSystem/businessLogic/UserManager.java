/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userSystem.businessLogic;

import businessEntityDomain.User;

/**
 *
 * @author Sander van Belleghem
 */
public interface UserManager {

	public boolean login(String username, String password);

	public boolean isLeidinggevende();

	public User getUser(String username, String password);
}
