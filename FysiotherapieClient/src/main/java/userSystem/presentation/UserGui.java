/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userSystem.presentation;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import userSystem.businessLogic.UserManager;
import behandeling.businessLogic.BehandelingManagerImpl;
import behandeling.presentation.BehandelingUI;
import businessEntityDomain.ImmutableUser;

/**
 *
 * @author Sander van Belleghem
 */
public class UserGui {

	private final UserManager userManager;
	private final JPanel panel = new JPanel();

	private final JPanel loginPanel = new JPanel(new GridLayout(3, 2));

	private final JTextField inputFieldUser = new JTextField(10);
	private final JPasswordField inputFieldPass = new JPasswordField(10);
	private final JButton loginButton = new JButton("Login");
	private JPanel cardPanel;
	private CardLayout cardLayout;

	public UserGui(UserManager userManagerObj, CardLayout cardlayout,
			JPanel cardpanel) {

		cardLayout = cardlayout;
		cardPanel = cardpanel;

		userManager = userManagerObj;

		initComponents();

		// Set focus on inputfield
		inputFieldUser.requestFocusInWindow();
	}

	private void initComponents() {

		loginPanel.add(new JLabel("Gebruikersnaam: "));
		loginPanel.add(inputFieldUser);
		loginPanel.add(new JLabel("Wachtwoord: "));
		loginPanel.add(inputFieldPass);
		loginPanel.add(new JLabel());
		loginPanel.add(loginButton);
		loginPanel.setBackground(new Color(211, 211, 211, 246));
		loginPanel.setPreferredSize(new Dimension(300, 100));
		loginPanel.setMaximumSize(loginPanel.getPreferredSize());
		loginPanel.setBorder(BorderFactory.createEmptyBorder(15, 25, 15, 25));

		// Add mainPanel to Panel
		panel.add(loginPanel);

		loginButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				loginButtonActionPerformed(evt);
			}
		});
	}

	private void loginButtonActionPerformed(java.awt.event.ActionEvent evt) {
		String username = inputFieldUser.getText();
		@SuppressWarnings("deprecation")
		String password = inputFieldPass.getText();

		if (username.length() > 0 && password.length() > 0) {
			if (userManager.login(username, password)) {
				inputFieldUser.setText("");
				inputFieldPass.setText("");

				ImmutableUser user = userManager.getUser(username, password);
				String userId = user.getUserNumber();
				
				if (!userManager.isLeidinggevende()) {
					
					BehandelingUI behandelingGui = new BehandelingUI(new BehandelingManagerImpl(userId),
							cardLayout, cardPanel);
					
					cardPanel.add(behandelingGui.getStartPanel(), "behandelingGui");
					
					cardLayout.show(cardPanel, "fysiotherapeutStartGui");
				} else {
					cardLayout.show(cardPanel, "leiderGui");
				}
			} else {
				JOptionPane.showMessageDialog(null,
						"Gebruikersnaam, wachtwoord combinatie niet gevonden.");
			}
		} else {
			JOptionPane
					.showMessageDialog(null,
							"Zowel de gebruikersnaam als wachtwoord dienen ingevuld te zijn.");
		}
	}

	public JPanel getPanel() {
		return panel;
	}
}
