package opstart.main;

import opstart.presentation.MainGui;

/**
 *
 * @author Sander van Belleghem
 */
public class Main {

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		MainGui mainGui = new MainGui();
		mainGui.setVisible(true);

	}
}
