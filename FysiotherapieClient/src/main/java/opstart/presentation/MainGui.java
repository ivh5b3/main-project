/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opstart.presentation;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import userSystem.businessLogic.UserManagerImpl;
import userSystem.presentation.UserGui;
import behandeling.businessLogic.BehandelingManagerImpl;
import behandeling.presentation.BehandelingUI;
import fysiotherapeut.businessLogic.FysiotherapeutManagerImpl;
import fysiotherapeut.presentation.FysiotherapeutUi;
import fysiotherapiepraktijk.businessLogic.PraktijkManagerImpl;
import fysiotherapiepraktijk.presentation.PraktijkUi;

/**
 *
 * @author Sander van Belleghem
 */
public class MainGui extends JFrame {

	private JPanel cardPanel = new JPanel();
	private CardLayout cardLayout = new CardLayout();

	private final JPanel cPanel = new JPanel();
	private final JPanel cSubPanel = new JPanel(new GridLayout(10, 1));
	final JPanel cMainPanel = new JPanel(new BorderLayout());

	private final JPanel cPanel2 = new JPanel();
	private final JPanel cSubPanel2 = new JPanel(new GridLayout(10, 1));
	final JPanel cMainPanel2 = new JPanel(new BorderLayout());
	
	public MainGui() {

		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			System.out.println("Something went wrong:" + e.getMessage());
		}

		initComponents();
		pack();
	}

	/**
	 * Creates MainGui
	 */
	private void initComponents() {
		cardPanel.setLayout(cardLayout);

		UserGui userGui = new UserGui(new UserManagerImpl(), cardLayout,
				cardPanel);
		FysiotherapeutUi fysiotherapeutGui = new FysiotherapeutUi(
				new FysiotherapeutManagerImpl(), cardLayout, cardPanel);
		PraktijkUi praktijkGui = new PraktijkUi(new PraktijkManagerImpl(),
				cardLayout, cardPanel);
		
		cardPanel.add(userGui.getPanel(), "login");
		cardPanel.add(getLeiderStartPanel(), "leiderGui");
		cardPanel.add(getFysiotherapeutStartPanel(), "fysiotherapeutStartGui");
		cardPanel.add(fysiotherapeutGui.getStartPanel(), "fysiotherapeutGui");
		cardPanel.add(praktijkGui.getStartPanel(), "praktijkGui");

		getContentPane().add(cardPanel, BorderLayout.CENTER);

		cardLayout.show(cardPanel, "login");

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Set Window to Full screen
		setExtendedState(MAXIMIZED_BOTH);
		// setUndecorated(true);
	}

	public JPanel getLeiderStartPanel() {

		JLabel welkomLabel = new JLabel("Welkom!");
		JLabel uitlegLabel = new JLabel(
				"Kies het gewenste systeem uit de rij onder deze tekst. Vervolges wordt u doorgestuurd");

		JButton uitlogButton = new JButton("Uitloggen");
		JButton praktijkButton = new JButton("Praktijk");
		JButton fysiotherapeutButton = new JButton("Fysiotherapeut");
		
		cSubPanel.add(welkomLabel);
		cSubPanel.add(uitlegLabel);
		cSubPanel.add(praktijkButton);
		cSubPanel.add(fysiotherapeutButton);
		cSubPanel.add(uitlogButton);

		cMainPanel.add(cSubPanel);
		cMainPanel.setPreferredSize(new Dimension(600, 500));
		cMainPanel.setMaximumSize(cPanel.getPreferredSize());
		cMainPanel.setBorder(BorderFactory.createEmptyBorder(50, 25, 50, 25));

		cPanel.add(cMainPanel);

		praktijkButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				showPanel("praktijkGui");
			}
		});

		fysiotherapeutButton
				.addActionListener(new java.awt.event.ActionListener() {
					@Override
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						showPanel("fysiotherapeutGui");
					}
				});

		uitlogButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				showPanel("login");
			}
		});

		return cPanel;
	}

	public JPanel getFysiotherapeutStartPanel() {

		JLabel welkomLabel = new JLabel("Welkom!");
		JLabel uitlegLabel = new JLabel(
				"Kies het gewenste systeem uit de rij onder deze tekst. Vervolges wordt u doorgestuurd");
		JButton uitlogButton = new JButton("Uitloggen");
		JButton behandelingButton = new JButton("Behandeling");
			
		cSubPanel2.add(welkomLabel);
		cSubPanel2.add(uitlegLabel);
		cSubPanel2.add(behandelingButton);
		cSubPanel2.add(uitlogButton);

		cMainPanel2.add(cSubPanel2);
		cMainPanel2.setPreferredSize(new Dimension(600, 500));
		cMainPanel2.setMaximumSize(cPanel.getPreferredSize());
		cMainPanel2.setBorder(BorderFactory.createEmptyBorder(50, 25, 50, 25));

		cPanel2.add(cMainPanel2);

		behandelingButton
				.addActionListener(new java.awt.event.ActionListener() {
					@Override
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						showPanel("behandelingGui");
					}
				});


		uitlogButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				showPanel("login");
			}
		});

		return cPanel2;
	}

	/**
	 * Method for setting the active cardLayout for switching between Gui's
	 *
	 * @param panel
	 *            JPanel
	 */
	public void showPanel(String panel) {
		cardLayout.show(cardPanel, panel);
	}
}
