/**
 * 
 */
package datastorage;

import interfaces.BehandelCodeDAOInf;
import interfaces.BehandelTrajectDAOInf;
import interfaces.FysiotherapeutDAOInf;
import interfaces.PatientDAOInf;
import interfaces.PraktijkDAOInf;
import interfaces.SessieDAOInf;
import interfaces.UserDAOInf;

/**
 * @author robbie, Sander van Belleghem
 *
 */
public abstract class DAOFactory {

	public static DAOFactory getDAOFactory(String factoryClassName) {

		DAOFactory factoryInstance = null;

		try {
			Class<?> factoryClass = Class.forName(factoryClassName);

			factoryInstance = (DAOFactory) factoryClass.newInstance();
		} catch (ClassNotFoundException e) {
			System.out.println("ClassNotFoundException: " + e.getMessage());
		} catch (Exception e) {
			System.out.println("Exception" + e.getMessage());
		}

		return factoryInstance;
	}

	public abstract BehandelTrajectDAOInf getBehandelTrajectDAO();

	public abstract FysiotherapeutDAOInf getFysiotherapeutDAO();

	public abstract PraktijkDAOInf getPraktijkDAO();

	public abstract SessieDAOInf getSessieDAO();

	public abstract UserDAOInf getUserDAO();

	public abstract PatientDAOInf getPatientDAO();
	
	public abstract BehandelCodeDAOInf getBehandelCodeDAO();
}
