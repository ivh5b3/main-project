/**
 * 
 */
package datastorage;

import interfaces.BehandelCodeDAOInf;
import interfaces.BehandelTrajectDAOInf;
import interfaces.FysiotherapeutDAOInf;
import interfaces.PatientDAOInf;
import interfaces.PraktijkDAOInf;
import interfaces.SessieDAOInf;
import interfaces.UserDAOInf;

import java.util.Properties;

import rmi.RmiConnection;
import util.Settings;

/**
 * @author robbie, Sander van Belleghem
 *
 */
public class XMLDOMDAOFactory extends DAOFactory {

	private RmiConnection connection;

	public XMLDOMDAOFactory() {

		Properties properties = new Settings();

		connection = new RmiConnection(properties.getProperty("hostname"));
	}

	@Override
	public SessieDAOInf getSessieDAO() {
		return (SessieDAOInf) connection.getService(SessieDAOInf.servicename);
	}

	@Override
	public BehandelTrajectDAOInf getBehandelTrajectDAO() {
		return (BehandelTrajectDAOInf) connection
				.getService(BehandelTrajectDAOInf.servicename);
	}

	@Override
	public FysiotherapeutDAOInf getFysiotherapeutDAO() {
		return (FysiotherapeutDAOInf) connection
				.getService(FysiotherapeutDAOInf.servicename);
	}

	@Override
	public PraktijkDAOInf getPraktijkDAO() {
		return (PraktijkDAOInf) connection
				.getService(PraktijkDAOInf.servicename);
	}

	@Override
	public UserDAOInf getUserDAO() {
		return (UserDAOInf) connection.getService(UserDAOInf.servicename);
	}

	@Override
	public PatientDAOInf getPatientDAO() {
		return (PatientDAOInf) connection.getService(PatientDAOInf.servicename);
	}

	@Override
	public BehandelCodeDAOInf getBehandelCodeDAO() {
		return (BehandelCodeDAOInf) connection
				.getService(BehandelCodeDAOInf.servicename);
	}

}
