/**
 * 
 */
package behandeling.businessLogic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import behandeling.presentation.ComboBoxItem;
import businessEntityDomain.ImmutableBehandelTraject;
import businessEntityDomain.ImmutablePatient;
import businessEntityDomain.ImmutableSessie;

/**
 * @author robbie
 *
 */

public interface BehandelingManager {

	public void setDatumFilter(Timestamp beginDatum);

	public void setDatumFilter(Timestamp beginDatum, Timestamp eindDatum);

	public void setDefaultFilter();

	public void setStatusFilter(String status);

	public LinkedHashMap<ImmutableSessie, ImmutablePatient> getSessies();

	public ArrayList<? extends ImmutableBehandelTraject> getBehandelingen();

	public ImmutableBehandelTraject getBehandelTraject(int id);

	public boolean insertBehandelTraject(String patientId, String behandelCodes);

	public ImmutablePatient getKlant(String bsn);

	public boolean addSessie(Timestamp beginDatum, int behandelTrajectId,
			String opmerking);

	public boolean deleteSessie(int sessieNummer);

	public boolean deleteBehandelTraject(int behandelTrajectId);

	public ImmutableSessie getSessie(int sessieNummer);

	public boolean updateSessie(int sessieNummer, Timestamp beginDatum,
			String status, String opmerking);

	public ComboBoxItem[] getBehandelCodes();

	public LinkedHashMap<ImmutableSessie, ImmutablePatient> getSessiesByBehandelTraject(
			int behandelTrajectId);

	public LinkedHashMap<ImmutableSessie, ImmutablePatient> getSessies(
			boolean gebruikSessieFilters);

	public LinkedHashMap<ImmutableSessie, ImmutablePatient> getSessiesByBehandelTraject(
			int behandelTrajectId, boolean gebruikSessieFilters);

}