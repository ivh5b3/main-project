/**
 * 
 */
package behandeling.businessLogic;

import interfaces.BehandelCodeDAOInf;
import interfaces.BehandelTrajectDAOInf;
import interfaces.FysiotherapeutDAOInf;
import interfaces.PatientDAOInf;
import interfaces.SessieDAOInf;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Properties;

import util.Settings;
import behandeling.presentation.ComboBoxItem;
import businessEntityDomain.BehandelCode;
import businessEntityDomain.BehandelTraject;
import businessEntityDomain.DatumFilter;
import businessEntityDomain.FilterInf;
import businessEntityDomain.Fysiotherapeut;
import businessEntityDomain.FysiotherapeutFilter;
import businessEntityDomain.ImmutableBehandelTraject;
import businessEntityDomain.ImmutablePatient;
import businessEntityDomain.ImmutableSessie;
import businessEntityDomain.Sessie;
import businessEntityDomain.StatusFilter;
import datastorage.DAOFactory;

/**
 * @author robbie
 *
 */
public class BehandelingManagerImpl implements BehandelingManager {

	private ArrayList<FilterInf> filters;
	private DAOFactory daoFactory;
	private Fysiotherapeut fysio;

	public BehandelingManagerImpl(String fysioBSN) {
		try {

			Properties properties = new Settings();

			daoFactory = DAOFactory.getDAOFactory(properties
					.getProperty("daofactory"));
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		FysiotherapeutDAOInf fysioDAO = daoFactory.getFysiotherapeutDAO();

		try {
			fysio = fysioDAO.get(fysioBSN);
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());

			fysio = null;
		}

		this.filters = new ArrayList<FilterInf>();

		// De standaard filter moet ingesteld worden
		setDefaultFilter();
	}

	private void setFilter(FilterInf filter) {
		// Op dit moment kan er nog maar 1 filter (datum of status) per keer
		// worden toegepast
		this.filters.clear();
		this.filters.add(filter);
		this.filters.add(new FysiotherapeutFilter(fysio));
	}

	private void setDatumFilter(DatumFilter datumFilter) {
		setFilter(datumFilter);
	}

	@Override
	public void setDatumFilter(Timestamp beginDatum) {
		setDatumFilter(new DatumFilter(beginDatum));
	}

	@Override
	public void setDatumFilter(Timestamp beginDatum, Timestamp eindDatum) {
		setDatumFilter(new DatumFilter(beginDatum, eindDatum, true));
	}

	@Override
	public void setDefaultFilter() {
		Date date = new Date();
		Timestamp beginDatum = new Timestamp(date.getTime());

		setDatumFilter(new DatumFilter(beginDatum, true));
	}

	@Override
	public void setStatusFilter(String status) {
		setFilter(new StatusFilter(status));
	}

	private FilterInf[] getFilterArray() {
		return filters.toArray(new FilterInf[filters.size()]);
	}

	@Override
	public ImmutablePatient getKlant(String bsn) {
		PatientDAOInf dao = daoFactory.getPatientDAO();

		try {
			return dao.getByBSN(bsn);
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());

			return null;
		}
	}

	private BehandelTraject getBehandelTrajectFromDAO(int id) {
		BehandelTrajectDAOInf dao = daoFactory.getBehandelTrajectDAO();

		try {
			return dao.get(id);
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());

			return null;
		}
	}

	@Override
	public ImmutableBehandelTraject getBehandelTraject(int id) {
		return getBehandelTrajectFromDAO(id);
	}

	@Override
	public ArrayList<? extends ImmutableBehandelTraject> getBehandelingen() {
		BehandelTrajectDAOInf dao = daoFactory.getBehandelTrajectDAO();

		try {
			ArrayList<? extends ImmutableBehandelTraject> behandelingen = dao
					.getBehandelingen(getFilterArray());

			Collections.sort(behandelingen,
					new Comparator<ImmutableBehandelTraject>() {
						@Override
						public int compare(ImmutableBehandelTraject s1,
								ImmutableBehandelTraject s2) {
							return s2.getId() - s1.getId();
						}
					});

			return behandelingen;
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());

			return null;
		}
	}

	@Override
	public boolean insertBehandelTraject(String patientId, String behandelCode) {
		BehandelTrajectDAOInf behandelTrajectDAO = daoFactory
				.getBehandelTrajectDAO();

		try {
			PatientDAOInf patientDAO = daoFactory.getPatientDAO();
			ImmutablePatient patient = patientDAO.getByBSN(patientId);

			BehandelCodeDAOInf behandelCodeDAO = daoFactory
					.getBehandelCodeDAO();
			BehandelCode behandelCodeObj = behandelCodeDAO.get(behandelCode);

			BehandelTraject behandelTraject = new BehandelTraject(patient,
					fysio, behandelCodeObj);

			return behandelTrajectDAO.insert(behandelTraject);
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());

			return false;
		}
	}

	@Override
	public boolean deleteBehandelTraject(int behandelTrajectId) {
		BehandelTrajectDAOInf dao = daoFactory.getBehandelTrajectDAO();

		try {
			return dao.delete(getBehandelTrajectFromDAO(behandelTrajectId));
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());

			return false;
		}
	}

	@Override
	public LinkedHashMap<ImmutableSessie, ImmutablePatient> getSessies(
			boolean gebruikSessieFilters) {
		return createSortedSessieMap(getBehandelingen(), gebruikSessieFilters);
	}

	@Override
	public LinkedHashMap<ImmutableSessie, ImmutablePatient> getSessies() {
		return getSessies(true);
	}

	@Override
	public LinkedHashMap<ImmutableSessie, ImmutablePatient> getSessiesByBehandelTraject(
			int behandelTrajectId, boolean gebruikSessieFilters) {

		ArrayList<ImmutableBehandelTraject> behandelingen = new ArrayList<ImmutableBehandelTraject>();
		behandelingen.add(getBehandelTraject(behandelTrajectId));

		return createSortedSessieMap(behandelingen, gebruikSessieFilters);
	}

	@Override
	public LinkedHashMap<ImmutableSessie, ImmutablePatient> getSessiesByBehandelTraject(
			int behandelTrajectId) {
		return getSessiesByBehandelTraject(behandelTrajectId, true);
	}

	private LinkedHashMap<ImmutableSessie, ImmutablePatient> createSortedSessieMap(
			ArrayList<? extends ImmutableBehandelTraject> behandelingen,
			boolean gebruikSessieFilters) {
		HashMap<ImmutableSessie, ImmutablePatient> sessieMap = new HashMap<ImmutableSessie, ImmutablePatient>();

		for (ImmutableBehandelTraject behandeling : behandelingen) {

			ArrayList<ImmutableSessie> sessies = (gebruikSessieFilters) ? behandeling
					.getSessies(getFilterArray()) : behandeling.getSessies();

			for (ImmutableSessie sessie : sessies) {
				sessieMap.put(sessie, behandeling.getPatient());
			}
		}

		ArrayList<ImmutableSessie> sessieList = new ArrayList<ImmutableSessie>(
				sessieMap.keySet());

		Collections.sort(sessieList, new Comparator<ImmutableSessie>() {
			@Override
			public int compare(ImmutableSessie s1, ImmutableSessie s2) {
				return s1.getBeginDatumTijd().compareTo(s2.getBeginDatumTijd());
			}
		});

		LinkedHashMap<ImmutableSessie, ImmutablePatient> sortedMap = new LinkedHashMap<ImmutableSessie, ImmutablePatient>();

		for (ImmutableSessie sessie : sessieList) {
			sortedMap.put(sessie, sessieMap.get(sessie));
		}

		return sortedMap;
	}

	@Override
	public boolean addSessie(Timestamp beginDatum, int behandelTrajectId,
			String opmerking) {
		BehandelTrajectDAOInf behandelTrajectDAO = daoFactory
				.getBehandelTrajectDAO();

		try {
			ImmutableBehandelTraject behandelTraject = getBehandelTraject(behandelTrajectId);

			Timestamp eindDatum = new Timestamp(beginDatum.getTime()
					+ behandelTraject.getBehandelCode().getSessieDuur());

			Sessie sessie = new Sessie(beginDatum, eindDatum, "status",
					behandelTrajectId, opmerking);

			if (behandelTrajectDAO.isMogelijk(behandelTraject, sessie)) {
				SessieDAOInf sessieDAO = daoFactory.getSessieDAO();

				return sessieDAO.insert(sessie);
			}
			System.out.println("Sessie kan niet gepland worden");
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());
		}

		return false;
	}

	@Override
	public boolean deleteSessie(int sessieNummer) {
		SessieDAOInf sessieDAO = daoFactory.getSessieDAO();

		try {
			return sessieDAO.delete(getSessieFromDAO(sessieNummer));
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());

			return false;
		}
	}

	private Sessie getSessieFromDAO(int sessieNummer) {
		SessieDAOInf sessieDAO = daoFactory.getSessieDAO();

		try {
			return sessieDAO.get(sessieNummer);
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());

			return null;
		}
	}

	@Override
	public ImmutableSessie getSessie(int sessieNummer) {
		return getSessieFromDAO(sessieNummer);
	}

	@Override
	public boolean updateSessie(int sessieNummer, Timestamp beginDatum, String status, String opmerking) {
		SessieDAOInf sessieDAO = daoFactory.getSessieDAO();
		BehandelTrajectDAOInf behandelTrajectDAO = daoFactory.getBehandelTrajectDAO();

		try {
			Sessie sessie = getSessieFromDAO(sessieNummer);
			
			BehandelTraject behandelTraject = behandelTrajectDAO.get(sessie.getBehandelTrajectId());
			
			Timestamp eindDatum = new Timestamp(beginDatum.getTime()
					+ behandelTraject.getBehandelCode().getSessieDuur());

			sessie.update(beginDatum, eindDatum, status, opmerking);
			
			return sessieDAO.update(sessie);
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());

			return false;
		}
	}

	@Override
	public ComboBoxItem[] getBehandelCodes() {

		try {
			BehandelCodeDAOInf behandelCodeDAO = daoFactory
					.getBehandelCodeDAO();

			ArrayList<BehandelCode> behandelCodeObjs = behandelCodeDAO.getAll();

			ComboBoxItem[] behandelCodes = new ComboBoxItem[behandelCodeObjs
					.size()];

			int i = 0;

			for (BehandelCode behandelCode : behandelCodeObjs) {
				behandelCodes[i] = new ComboBoxItem(
						behandelCode.getBehandelCode(),
						behandelCode.getBehandelingNaam() + " ("
								+ behandelCode.getBehandelCode() + ")");

				i++;
			}

			return behandelCodes;
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());
		}

		return null;
	}
}