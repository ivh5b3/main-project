/**
 * 
 */
package behandeling.presentation;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import behandeling.businessLogic.BehandelingManager;
import businessEntityDomain.ImmutableBehandelCode;
import businessEntityDomain.ImmutableBehandelTraject;
import businessEntityDomain.ImmutablePatient;
import businessEntityDomain.ImmutableSessie;

/**
 * @author robbie
 *
 */
public class BehandelingUI {

	private BehandelingManager manager;
	private JTable table;
	private ArrayList<Integer> rowSessieNummer;
	private ArrayList<Integer> rowBehandelTrajectNummer;
	private JPanel cardPanel;
	private CardLayout cardLayout;
	private JTextField beginDatum;
	private JTextField behandelTraject;
	private JTextField opmerking;
	private JComboBox<ComboBoxItem> behandelCode;
	private JTextField patient;
	private boolean behandelTrajectenScherm;
	private JTextField filterStatusField;
	private JTextField filterField;
	private JTextField filterEindField;
	private JLabel lblFoutmelding = new JLabel("");
	private JLabel lblError = new JLabel("");
	private JButton btnToevoegen;
	private JButton btnAnnuleren;
	private JRadioButton statusButton;
	private JRadioButton datumButton;
	private JPanel popupPanel;
	private JPanel defaultPanel;
	private boolean popupSessieInvoerenIsAdded;

	public BehandelingUI(BehandelingManager manager, CardLayout cardlayout,
			JPanel cardpanel) {
		this.manager = manager;
		cardLayout = cardlayout;
		cardPanel = cardpanel;

		rowSessieNummer = new ArrayList<Integer>();
		rowBehandelTrajectNummer = new ArrayList<Integer>();

		behandelTrajectenScherm = true;
	}

	public JPanel getStartPanel() {
		JPanel panel = new JPanel();

		table = new JTable(getBehandelTrajectTableModel());

		table.setRowSelectionAllowed(true);
		table.setRowHeight(25);
		table.getTableHeader().setReorderingAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.revalidate();
		scrollPane.setViewportView(table);

		panel.setBounds(100, 100, 800, 600);

		JButton btnTerug = new JButton("< Terug");
		btnTerug.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cardLayout.show(cardPanel, "fysiotherapeutStartGui");
			}
		});

		JButton btnSessieToevoegen = new JButton("Sessie Toevoegen");
		btnSessieToevoegen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (table.getSelectedRow() == -1) {
					JOptionPane
							.showMessageDialog(
									null,
									"Selecteer een behandeltraject waar de sessie aan moet worden gekoppeld.",
									"Fout: Toevoegen",
									JOptionPane.INFORMATION_MESSAGE);
				} else {
					if (!popupSessieInvoerenIsAdded) {
						cardPanel.add(getPopupSessieInvoerenPanel(),
								"Sessie toevoegen");		
						
						popupSessieInvoerenIsAdded = true;
					}
					cardLayout.show(cardPanel, "Sessie toevoegen");
				}
			}
		});

		JButton btnToevoegen = new JButton("Toevoegen");
		btnToevoegen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				cardPanel.add(getPopupBehandelTrajectInvoerenPanel(),
						"Behandeltraject toevoegen");
				cardLayout.show(cardPanel, "Behandeltraject toevoegen");
			}
		});

		JButton btnWijzigen = new JButton("Wijzigen");
		btnWijzigen.setVisible(false);
		btnWijzigen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String text = (behandelTrajectenScherm) ? "behandeltraject"
						: "sessie";

				if (table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null, "Er is nog geen "
							+ text + " geselecteerd!",
							"Fout: Wijzigen " + text,
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					ImmutableSessie selectedSessie = manager
							.getSessie(rowSessieNummer.get(table
									.getSelectedRow()));

					if (selectedSessie == null) {
						JOptionPane
								.showConfirmDialog(
										null,
										"Er ging iets fout bij het selecteren van een sessie. Probeer opnieuw.",
										"Fout bij selecteren",
										JOptionPane.PLAIN_MESSAGE);
						table.setModel(getSessieTableModel());
					} else {
						cardPanel.add(
								getPopupSessieWijzigenPanel(selectedSessie),
								"Behandeltraject wijzigen");
						cardLayout.show(cardPanel, "Behandeltraject wijzigen");
					}
				}
			}
		});

		JPanel panel2 = new JPanel();

		JButton btnVerwijder = new JButton("Verwijder");
		btnVerwijder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String text = (behandelTrajectenScherm) ? "behandeltraject"
						: "sessie";

				if (table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null, "Er is nog geen "
							+ text + " geselecteerd!", "Fout: Verwijderen "
							+ text, JOptionPane.INFORMATION_MESSAGE);
				} else {

					int dialogResult = JOptionPane.showConfirmDialog(null,
							"Weet u zeker dat u deze " + text
									+ " wilt verwijderen?", "Waarschuwing",
							JOptionPane.YES_NO_OPTION);
					if (dialogResult == JOptionPane.YES_OPTION) {

						if (behandelTrajectenScherm) {
							if (rowBehandelTrajectNummer.get(table
									.getSelectedRow()) == null) {
								JOptionPane
										.showConfirmDialog(
												null,
												"Er ging iets fout bij het selecteren van een sessie. "
												+ "Probeer opnieuw.",
												"Fout bij selecteren",
												JOptionPane.PLAIN_MESSAGE);
								table.setModel(getSessieTableModel());
							} else {
								manager.deleteBehandelTraject(rowBehandelTrajectNummer
										.get(table.getSelectedRow()));

								table.setModel(getBehandelTrajectTableModel());
							}
						} else {
							if (rowSessieNummer.get(table.getSelectedRow()) == null) {
								JOptionPane
										.showConfirmDialog(
												null,
												"Er ging iets fout bij het selecteren van een sessie. Probeer opnieuw.",
												"Fout bij selecteren",
												JOptionPane.PLAIN_MESSAGE);
								table.setModel(getSessieTableModel());
							} else {
								manager.deleteSessie(rowSessieNummer.get(table
										.getSelectedRow()));

								table.setModel(getSessieTableModel());
							}
						}

						table.revalidate();
						table.repaint();
					}
				}
			}
		});

		JButton btnVerwijderFilter = new JButton("Verwijder filter");
		btnVerwijderFilter.setVisible(false);
		btnVerwijderFilter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				int result = JOptionPane.showConfirmDialog(null,
						"Weet u zeker dat u deze filter wilt verwijderen?",
						"Waarschuwing", JOptionPane.YES_NO_OPTION);

				if (result == JOptionPane.YES_OPTION) {

					btnVerwijderFilter.setVisible(false);

					manager.setDefaultFilter();
				}

				table.setModel(getSessieTableModel());

				table.revalidate();
				table.repaint();
			}
		});

		JButton btnFilter = new JButton("Filter");
		btnFilter.setVisible(false);
		btnFilter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				final JOptionPane optionPane = new JOptionPane(
						getPopupFilterPanel(), JOptionPane.PLAIN_MESSAGE,
						JOptionPane.DEFAULT_OPTION);

				final JDialog dialog = new JDialog();
				dialog.setContentPane(optionPane);
				dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

				optionPane
						.addPropertyChangeListener(new PropertyChangeListener() {

							public void propertyChange(PropertyChangeEvent e) {
								String prop = e.getPropertyName();

								if (dialog.isVisible()
										&& (e.getSource() == optionPane)
										&& (prop.equals(JOptionPane.VALUE_PROPERTY))) {
									
									setFilter();
									btnVerwijderFilter.setVisible(true);
									optionPane.getRootFrame().dispose();

									table.setModel(getSessieTableModel());
									table.revalidate();
									table.repaint();

								}
							}
						});
				dialog.pack();
				dialog.setVisible(true);
			}
		});

		JButton btnSessies = new JButton("Sessies");

		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(6)
																		.addComponent(
																				panel2,
																				GroupLayout.DEFAULT_SIZE,
																				788,
																				Short.MAX_VALUE))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				btnTerug)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				btnSessies)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnToevoegen)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnSessieToevoegen)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnWijzigen)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnVerwijder)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnFilter)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnVerwijderFilter)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addContainerGap()))));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										groupLayout
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(btnTerug)
												.addComponent(btnSessies)
												.addComponent(btnToevoegen)
												.addComponent(
														btnSessieToevoegen)
												.addComponent(btnWijzigen)
												.addComponent(btnVerwijder)
												.addComponent(btnFilter)
												.addComponent(
														btnVerwijderFilter))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(panel2,
										GroupLayout.PREFERRED_SIZE, 522,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(15, Short.MAX_VALUE)));
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));
		panel2.add(scrollPane);
		panel.setLayout(groupLayout);

		btnSessies.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				if (behandelTrajectenScherm) {
					behandelTrajectenScherm = false;

					btnSessieToevoegen.setVisible(false);
					btnToevoegen.setVisible(false);
					btnFilter.setVisible(true);
					btnSessies.setText("Behandeltrajecten");
					btnWijzigen.setVisible(true);

					panel.revalidate();
					panel.repaint();

					table.setModel(getSessieTableModel());
					table.revalidate();
					table.repaint();
				} else {
					behandelTrajectenScherm = true;

					btnSessieToevoegen.setVisible(true);
					btnToevoegen.setVisible(true);
					btnFilter.setVisible(false);
					btnVerwijderFilter.setVisible(false);
					btnSessies.setText("Sessies");
					btnWijzigen.setVisible(false);

					panel.revalidate();
					panel.repaint();

					table.setModel(getBehandelTrajectTableModel());
					table.revalidate();
					table.repaint();
				}

			}
		});

		return panel;
	}

	public DefaultTableModel getSessieTableModel() {

		String[] columnNames = new String[] { "Begin", "Eind", "Naam",
				"Geslacht", "Status", "Opmerking" };

		// todo: null controle toevoegen
		LinkedHashMap<ImmutableSessie, ImmutablePatient> behandelingen = manager
				.getSessies();

		String[][] data = new String[behandelingen.size()][columnNames.length];

		int row = 0;

		rowSessieNummer.clear();

		for (HashMap.Entry<ImmutableSessie, ImmutablePatient> entry : behandelingen
				.entrySet()) {

			ImmutableSessie sessie = entry.getKey();
			ImmutablePatient patient = entry.getValue();

			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"HH:mm dd-MM-yyyy");

			data[row][0] = dateFormat.format(sessie.getBeginDatumTijd());
			data[row][1] = dateFormat.format(sessie.getEindDatumTijd());
			data[row][2] = patient.getVoornaam() + " "
					+ patient.getAchternaam();
			data[row][3] = patient.getGeslacht();
			data[row][4] = sessie.getStatus();
			data[row][5] = sessie.getOpmerking();

			rowSessieNummer.add(sessie.getSessieNummer());

			row++;
		}

		DefaultTableModel tableModel = new DefaultTableModel(data, columnNames) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -1609904890424387918L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		return tableModel;
	}

	public DefaultTableModel getBehandelTrajectTableModel() {

		String[] columnNames = new String[] { "Behandelcode", "Behandeling",
				"patient", "Geplande sessies", "Aantal sessies" };

		// todo: null controle toevoegen
		ArrayList<? extends ImmutableBehandelTraject> behandelingen = manager
				.getBehandelingen();

		String[][] data = new String[behandelingen.size()][columnNames.length];

		int row = 0;

		for (ImmutableBehandelTraject behandeling : behandelingen) {

			ImmutablePatient patient = behandeling.getPatient();
			ImmutableBehandelCode behandelCode = behandeling.getBehandelCode();

			data[row][0] = behandelCode.getBehandelCode();
			data[row][1] = String.valueOf(behandelCode.getBehandelingNaam());
			data[row][2] = patient.getVoornaam() + " "
					+ patient.getAchternaam();
			data[row][3] = String.valueOf(behandeling.getSessies().size());
			data[row][4] = String.valueOf(behandelCode.getAantalSessies());

			rowBehandelTrajectNummer.add(behandeling.getId());

			row++;
		}

		DefaultTableModel tableModel = new DefaultTableModel(data, columnNames) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6474909126733467297L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		return tableModel;
	}

	public JPanel getPopupBehandelTrajectInvoerenPanel() {
		defaultPanel = new JPanel();

		popupPanel = new JPanel(new GridLayout(3, 2));

		popupPanel.setPreferredSize(new Dimension(500, 400));
		popupPanel.setMaximumSize(popupPanel.getPreferredSize());
		popupPanel.setBorder(BorderFactory.createEmptyBorder(15, 25, 15, 25));

		JLabel lblBehandelCode = new JLabel("Behandelcode");
		JLabel lblPatient = new JLabel("Patient");

		// todo: null controle toevoegen
		behandelCode = new JComboBox<ComboBoxItem>(manager.getBehandelCodes());
		patient = new JTextField(10);

		btnToevoegen = new JButton("invoeren");
		btnAnnuleren = new JButton("annuleren");

		btnToevoegen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ArrayList<String> fouten = voegBehandelTrajectToe();
				if (!fouten.isEmpty()) {
					String foutmeldingen = "";
					for (int i = 0; i < fouten.size(); i++) {
						foutmeldingen = foutmeldingen + fouten.get(i) + " \n";
					}

					JOptionPane.showMessageDialog(null, foutmeldingen,
							"Het volgende ging fout: ",
							JOptionPane.PLAIN_MESSAGE);
				} else {
					cardLayout.show(cardPanel, "behandelingGui");
					table.setModel(getBehandelTrajectTableModel());
				}

			}
		});
		btnAnnuleren.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cardLayout.show(cardPanel, "behandelingGui");
			}
		});

		popupPanel.add(btnAnnuleren);
		popupPanel.add(btnToevoegen);

		GroupLayout groupLayout = new GroupLayout(popupPanel);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																Alignment.LEADING,
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				lblBehandelCode)
																		.addGap(18)
																		.addComponent(
																				behandelCode,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblPatient)
																						.addComponent(
																								btnAnnuleren))
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING,
																								false)
																						.addGroup(
																								groupLayout
																										.createSequentialGroup()
																										.addGap(15)
																										.addGroup(
																												groupLayout
																														.createParallelGroup(
																																Alignment.LEADING)
																														.addGroup(
																																groupLayout
																																		.createSequentialGroup()
																																		.addComponent(
																																				btnToevoegen))
																														.addComponent(
																																behandelCode,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																patient,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE))))))
										.addGap(264)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblBehandelCode)
														.addComponent(
																behandelCode,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblPatient)
														.addComponent(
																patient,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(31)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																btnAnnuleren)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				btnToevoegen)))
										.addContainerGap(60, Short.MAX_VALUE)));
		popupPanel.setLayout(groupLayout);
		defaultPanel.add(popupPanel);

		return defaultPanel;
	}

	public JPanel getPopupSessieInvoerenPanel() {
		defaultPanel = new JPanel();

		popupPanel = new JPanel(new GridLayout(3, 2));

		popupPanel.setPreferredSize(new Dimension(500, 900));
		popupPanel.setMaximumSize(popupPanel.getPreferredSize());
		popupPanel.setBorder(BorderFactory.createEmptyBorder(15, 25, 15, 25));

		JLabel lblBeginDatum = new JLabel(
				"Begindatum en tijd (dd-mm-yyyy hh:mm)");
		JLabel lblOpmerking = new JLabel("Opmerking");

		beginDatum = new JTextField(20);
		opmerking = new JTextField(20);

		btnToevoegen = new JButton("invoeren");
		btnAnnuleren = new JButton("annuleren");

		btnToevoegen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ArrayList<String> fouten = voegSessieToe(rowBehandelTrajectNummer
						.get(table.getSelectedRow()));
				if (!fouten.isEmpty()) {
					String foutmeldingen = "";
					for (int i = 0; i < fouten.size(); i++) {
						foutmeldingen = foutmeldingen + fouten.get(i) + " \n";
					}

					JOptionPane.showMessageDialog(null, foutmeldingen,
							"Het volgende ging fout: ",
							JOptionPane.PLAIN_MESSAGE);
				} else {
					cardLayout.show(cardPanel, "behandelingGui");
					table.setModel(getBehandelTrajectTableModel());
				}

			}
		});
		btnAnnuleren.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cardLayout.show(cardPanel, "behandelingGui");
			}
		});

		popupPanel.add(btnAnnuleren);
		popupPanel.add(btnToevoegen);

		GroupLayout groupLayout = new GroupLayout(popupPanel);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																Alignment.LEADING,
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				lblError)
																		.addGap(18)
																		.addComponent(
																				lblFoutmelding,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE))

														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblBeginDatum)
																						.addComponent(
																								lblOpmerking)

																						.addComponent(
																								btnAnnuleren))
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING,
																								false)
																						.addGroup(
																								groupLayout
																										.createSequentialGroup()
																										.addGap(15)
																										.addGroup(
																												groupLayout
																														.createParallelGroup(
																																Alignment.LEADING)
																														.addGroup(
																																groupLayout
																																		.createSequentialGroup()
																																		.addComponent(
																																				btnToevoegen))
																														.addComponent(
																																lblFoutmelding,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																beginDatum,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																opmerking,
																																GroupLayout.PREFERRED_SIZE,
																																GroupLayout.DEFAULT_SIZE,
																																GroupLayout.PREFERRED_SIZE))))))
										.addGap(264)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGap(21)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblError)
														.addComponent(
																lblFoutmelding,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblBeginDatum)
														.addComponent(
																beginDatum,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblOpmerking)
														.addComponent(
																opmerking,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(31)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																btnAnnuleren)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				btnToevoegen)))
										.addContainerGap(60, Short.MAX_VALUE)));
		popupPanel.setLayout(groupLayout);
		defaultPanel.add(popupPanel);

		return defaultPanel;
	}

	public ArrayList<String> voegSessieToe(int behandelTrajectId) {
		ArrayList<String> errors = new ArrayList<String>();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		try {
			Date tBegindatum = dateFormat.parse(beginDatum.getText());

			Timestamp beginTimestamp = new Timestamp(tBegindatum.getTime());

			String tOpmerking = opmerking.getText();

			if (!manager.addSessie(beginTimestamp, behandelTrajectId,
					tOpmerking)) {
				errors.add("Fout tijdens het opslaan, probeer het later nog eens");
			}

		} catch (ParseException e) {
			errors.add("Begindatum format is niet correct, dit moet zijn dd-mm-yyyy hh:mm");
		}

		return errors;
	}

	public ArrayList<String> voegBehandelTrajectToe() {

		ArrayList<String> errors = new ArrayList<String>();

		Object item = behandelCode.getSelectedItem();
		String tBehandelCode = ((ComboBoxItem) item).getKey();

		if (tBehandelCode.isEmpty()) {
			errors.add("Behandelcode is leeg");
		}

		String tPatient = patient.getText();

		if (tPatient.isEmpty()) {
			errors.add("Patient bsn is leeg");
		}

		if (errors.isEmpty()) {
			if (manager.getKlant(tPatient) == null) {
				JOptionPane
						.showConfirmDialog(
								null,
								"Er ging iets fout bij het ophalen van een klant. Probeer opnieuw.",
								"Fout", JOptionPane.PLAIN_MESSAGE);
				table.setModel(getSessieTableModel());
			} else {
				ImmutablePatient patientObj = manager.getKlant(tPatient);

				if (patientObj != null) {
					if (!manager.insertBehandelTraject(patientObj.getBsn(),
							tBehandelCode)) {
						errors.add("Fout tijdens het opslaan, probeer het later nog eens");
					}
				} else {
					errors.add("Er is geen patient gevonden met dit BSN");
				}
			}
		}

		return errors;
	}

	public ArrayList<String> wijzigSessie(int sessieNummer) {
		ArrayList<String> errors = new ArrayList<String>();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		String tStatus = behandelTraject.getText();
		String tOpmerking = opmerking.getText();

		if (tStatus.isEmpty()) {
			errors.add("Status is niet ingevuld");
		}

		try {
			Date tBegindatum = dateFormat.parse(beginDatum.getText());

			Timestamp beginTimestamp = new Timestamp(tBegindatum.getTime());

			if (!manager.updateSessie(sessieNummer, beginTimestamp, tStatus,
					tOpmerking)) {
				errors.add("Fout tijdens het opslaan, probeer het laten nog eens");
			}
		} catch (ParseException e) {
			errors.add("Datum format is niet geldig gebruik: dd-mm-yyyy hh:mm");
		}

		return errors;
	}

	public JPanel getPopupSessieWijzigenPanel(ImmutableSessie sessie) {

		defaultPanel = new JPanel();

		popupPanel = new JPanel(new GridLayout(3, 2));

		popupPanel.setPreferredSize(new Dimension(300, 900));
		popupPanel.setMaximumSize(popupPanel.getPreferredSize());
		popupPanel.setBorder(BorderFactory.createEmptyBorder(15, 25, 15, 25));

		JLabel lblBeginDatum = new JLabel("Begindatum en tijd");
		JLabel lblBehandeling = new JLabel("Status");
		JLabel lblOpmerking = new JLabel("Opmerking");

		beginDatum = new JTextField(10);
		beginDatum.setText(new SimpleDateFormat("dd-MM-yyyy HH:mm")
				.format(sessie.getBeginDatumTijd()));

		behandelTraject = new JTextField(10);
		behandelTraject.setText(sessie.getStatus());

		opmerking = new JTextField(10);
		opmerking.setText(sessie.getOpmerking());

		btnToevoegen = new JButton("wijzigen");
		btnAnnuleren = new JButton("annuleren");

		btnToevoegen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ArrayList<String> fouten = wijzigSessie(rowSessieNummer
						.get(table.getSelectedRow()));
				if (!fouten.isEmpty()) {
					String foutmeldingen = "";
					for (int i = 0; i < fouten.size(); i++) {
						foutmeldingen = foutmeldingen + fouten.get(i) + " \n";
					}

					JOptionPane.showMessageDialog(null, foutmeldingen,
							"Het volgende ging fout: ",
							JOptionPane.PLAIN_MESSAGE);
				} else {
					cardLayout.show(cardPanel, "behandelingGui");
					table.setModel(getSessieTableModel());
				}

			}
		});
		btnAnnuleren.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cardLayout.show(cardPanel, "behandelingGui");
			}
		});

		popupPanel.add(btnAnnuleren);
		popupPanel.add(btnToevoegen);

		GroupLayout groupLayout = new GroupLayout(popupPanel);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.LEADING)

																		.addGroup(
																				Alignment.TRAILING,
																				groupLayout
																						.createSequentialGroup()
																						.addGroup(
																								groupLayout
																										.createParallelGroup(
																												Alignment.LEADING)
																										.addComponent(
																												lblBeginDatum)
																										.addComponent(
																												lblBehandeling)
																										.addComponent(
																												lblOpmerking)

																										.addComponent(
																												btnAnnuleren))
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addGroup(
																								groupLayout
																										.createParallelGroup(
																												Alignment.LEADING)

																										.addComponent(
																												beginDatum,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												behandelTraject,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												opmerking,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addGroup(
																												groupLayout
																														.createSequentialGroup()
																														.addComponent(
																																btnToevoegen))))))
										.addContainerGap(303, Short.MAX_VALUE)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()

										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblBeginDatum)
														.addComponent(
																beginDatum,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblBehandeling)
														.addComponent(
																behandelTraject,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblOpmerking)
														.addComponent(
																opmerking,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(31)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																btnAnnuleren)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				btnToevoegen)))
										.addContainerGap(60, Short.MAX_VALUE)));
		popupPanel.setLayout(groupLayout);
		defaultPanel.add(popupPanel);

		return defaultPanel;
	}

	public ArrayList<String> setFilter() {
		ArrayList<String> errors = new ArrayList<String>();

		if (statusButton.isSelected()) {
			String statusField = filterStatusField.getText();

			if (statusField.isEmpty()) {
				errors.add("Status is niet ingevuld");
			} else {
				manager.setStatusFilter(statusField);
			}
		} else if (datumButton.isSelected()) {
			String firstField = filterField.getText();
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"dd-MM-yyyy HH:mm");

			if (firstField.isEmpty()) {
				errors.add("Begindatum is niet ingevuld");
			} else {
				Timestamp beginTimestamp;

				try {
					Date tBegindatum = dateFormat.parse(firstField + " 00:00");
					beginTimestamp = new Timestamp(tBegindatum.getTime());
				} catch (ParseException e) {
					errors.add("BeginDatum voldoet niet aan: (dd-mm-yyyy)");

					return errors;
				}

				String secField = filterEindField.getText();

				if (secField.isEmpty()) {
					// Set filter with only start timestamp
					manager.setDatumFilter(beginTimestamp);
				} else {
					try {
						Date tEinddatum = dateFormat.parse(secField + " 23:59");
						Timestamp eindTimestamp = new Timestamp(
								tEinddatum.getTime());

						// Set filter with start and end timestamp
						manager.setDatumFilter(beginTimestamp, eindTimestamp);
					} catch (ParseException e) {
						errors.add("Einddatum voldoet niet aan: (dd-mm-yyyy)");
					}
				}
			}
		} else {
			errors.add("Er is geen keuze gemaakt tussen status of filter");
		}

		return errors;
	}

	public JPanel getPopupFilterPanel() {
		JPanel popupPanel = new JPanel();

		popupPanel.setBounds(100, 100, 800, 600);

		JLabel lblFilterChoice = new JLabel("Status of datum filter");
		JLabel lblFilterStatus = new JLabel("Voer een status in");
		JLabel lblFilterBegin = new JLabel("Voer datum (dd-mm-yyyy) in");
		JLabel lblFilterEind = new JLabel("Voer indien gewenst einddatum in");

		ButtonGroup group = new ButtonGroup();

		statusButton = new JRadioButton("status");
		statusButton.setSelected(true);

		datumButton = new JRadioButton("datum");

		group.add(statusButton);
		group.add(datumButton);

		filterStatusField = new JTextField(20);

		filterField = new JTextField(20);
		filterField.setEnabled(false);

		filterEindField = new JTextField(20);
		filterEindField.setEnabled(false);

		statusButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				filterField.setEnabled(false);
				filterEindField.setEnabled(false);
				filterStatusField.setEnabled(true);
			}

		});

		datumButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				filterStatusField.setEnabled(false);
				filterField.setEnabled(true);
				filterEindField.setEnabled(true);
			}

		});

		GroupLayout groupLayout = new GroupLayout(popupPanel);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.LEADING)

																		.addGroup(
																				Alignment.TRAILING,
																				groupLayout
																						.createSequentialGroup()
																						.addGroup(
																								groupLayout
																										.createParallelGroup(
																												Alignment.LEADING)
																										.addComponent(
																												lblFilterChoice)
																										.addComponent(
																												lblFilterStatus)
																										.addComponent(
																												lblFilterBegin)
																										.addComponent(
																												lblFilterEind))
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addGroup(
																								groupLayout
																										.createParallelGroup(
																												Alignment.LEADING)

																										.addGroup(
																												groupLayout
																														.createSequentialGroup()
																														.addComponent(
																																statusButton)
																														.addGap(18)
																														.addComponent(
																																datumButton))
																										.addComponent(
																												filterStatusField,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												filterField,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addComponent(
																												filterEindField,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)))))
										.addContainerGap(303, Short.MAX_VALUE)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()

										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																lblFilterChoice)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				statusButton)
																		.addComponent(
																				datumButton)))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblFilterStatus)
														.addComponent(
																filterStatusField,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblFilterBegin)
														.addComponent(
																filterField,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblFilterEind)
														.addComponent(
																filterEindField,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))));
		popupPanel.setLayout(groupLayout);

		return popupPanel;
	}
}