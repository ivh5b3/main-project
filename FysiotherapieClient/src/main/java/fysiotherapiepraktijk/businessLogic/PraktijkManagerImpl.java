package fysiotherapiepraktijk.businessLogic;

import interfaces.PraktijkDAOInf;

import java.rmi.RemoteException;
import java.util.Properties;

import util.Settings;
import businessEntityDomain.ImmutablePraktijk;
import businessEntityDomain.Praktijk;
import datastorage.DAOFactory;

public class PraktijkManagerImpl implements PraktijkManager {
	private DAOFactory factory;
	private PraktijkDAOInf praktijkDAO;

	public PraktijkManagerImpl() {
		try {
			Properties properties = new Settings();

			factory = DAOFactory.getDAOFactory(properties
					.getProperty("daofactory"));
			praktijkDAO = factory.getPraktijkDAO();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	private Praktijk getPraktijkFromDAO() {
		try {
			return praktijkDAO.get();
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());
			
			return null;
		}
	}
	
	@Override
	public ImmutablePraktijk getPraktijk() {
		return getPraktijkFromDAO();
	}
	
	@Override
	public boolean insertPraktijk(String naam, String adres, String postcode,
			String plaats, String telefoonNummer, String email, int kvk,
			String btwNummer) {

		try {
			return praktijkDAO.insert(new Praktijk(naam, adres, plaats,
					postcode, telefoonNummer, email, kvk, btwNummer));
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());
			
			return false;
		}
	}

	@Override
	public boolean updatePraktijk(String naam, String adres, String postcode,
			String plaats, String telefoonNummer, String email, int kvk,
			String btwNummer) {

		try {
			Praktijk praktijk = getPraktijkFromDAO();
			praktijk.update(naam, adres, postcode, plaats, telefoonNummer,
					email, kvk, btwNummer);

			return praktijkDAO.update(praktijk);
		} catch (RemoteException e) {
			System.out.println("RemoteException: " + e.getMessage());
			
			return false;
		}
	}
}
