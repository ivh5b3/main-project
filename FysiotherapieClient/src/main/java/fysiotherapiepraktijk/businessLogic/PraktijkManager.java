package fysiotherapiepraktijk.businessLogic;

import businessEntityDomain.ImmutablePraktijk;

public interface PraktijkManager {
	public ImmutablePraktijk getPraktijk();

	public boolean insertPraktijk(String naam, String adres, String postcode,
			String plaats, String telefoonNummer, String email, int kvk,
			String btwNummer);

	public boolean updatePraktijk(String naam, String adres, String postcode,
			String plaats, String telefoonNummer, String email, int kvk,
			String btwNummer);
}