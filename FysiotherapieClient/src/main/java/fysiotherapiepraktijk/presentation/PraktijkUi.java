package fysiotherapiepraktijk.presentation;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import businessEntityDomain.ImmutablePraktijk;
import fysiotherapiepraktijk.businessLogic.PraktijkManagerImpl;

public class PraktijkUi {

	private PraktijkManagerImpl manager;
	private JTable table;
	private DefaultTableModel tableModel;
	private JTextField naam;
	private JTextField adres;
	private JTextField postcode;
	private JTextField plaats;
	private JTextField telefoonnummer;
	private JTextField email;
	private JTextField kvkNummer;
	private JTextField btwNummer;
	private JPanel panel;
	private JPanel cardPanel;
	private CardLayout cardLayout;
	private JPanel praktijkWijzigenPopupPanel;
	private JPanel defaultPanel;
	private JButton btnToevoegen;
	private JButton btnAnnuleren;

	public PraktijkUi(PraktijkManagerImpl manager, CardLayout cardlayout,
			JPanel cardpanel) {
		this.manager = manager;

		cardLayout = cardlayout;
		cardPanel = cardpanel;
	}

	public JPanel getStartPanel() {
		panel = new JPanel();

		table = new JTable(getTableModel());

		table.setRowSelectionAllowed(true);
		table.setRowHeight(25);
		table.getTableHeader().setReorderingAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.revalidate();
		scrollPane.setViewportView(table);

		panel.setBounds(100, 100, 750, 500);

		JButton btnTerug = new JButton("< Terug");
		btnTerug.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cardLayout.show(cardPanel, "leiderGui");
			}
		});

		JButton btnWijzigen = new JButton("Wijzigen");
		btnWijzigen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				cardPanel.add(getPraktijkWijzigenPopup(manager),
						"Praktijk wijzigen");
				cardLayout.show(cardPanel, "Praktijk wijzigen");
			}
		});

		GroupLayout groupLayout = new GroupLayout(panel);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(6))

														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				btnTerug)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnWijzigen)
																		.addContainerGap())
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				scrollPane,
																				GroupLayout.DEFAULT_SIZE,
																				738,
																				Short.MAX_VALUE)))
										.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						groupLayout
								.createSequentialGroup()
								.addGroup(
										groupLayout
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(btnTerug)
												.addComponent(btnWijzigen))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(scrollPane,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(11, Short.MAX_VALUE)));
		panel.setLayout(groupLayout);
		return panel;
	}

	public DefaultTableModel getTableModel() {

		Object columnNames[] = { "Naam", "Adres", "Postcode", "Plaats",
				"Telefoonnummer", "E-mail", "Kvk", "btwnummer" };

		tableModel = new DefaultTableModel(columnNames, 0) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 5031087641547768314L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		tableModel.setRowCount(0);

		ImmutablePraktijk praktijk = manager.getPraktijk();

		Object[] rowData = { praktijk.getNaam(), praktijk.getAdres(),
				praktijk.getPostcode(), praktijk.getPlaats(),
				praktijk.getTelefoonnummer(), praktijk.getEmail(),
				praktijk.getKvk(), praktijk.getBtwNummer() };

		tableModel.addRow(rowData);

		return tableModel;
	}

	public JPanel getPraktijkWijzigenPopup(PraktijkManagerImpl manager) {
		this.manager = manager;

		defaultPanel = new JPanel();

		praktijkWijzigenPopupPanel = new JPanel(new GridLayout(3, 2));

		praktijkWijzigenPopupPanel.setPreferredSize(new Dimension(300, 900));
		praktijkWijzigenPopupPanel.setMaximumSize(praktijkWijzigenPopupPanel
				.getPreferredSize());
		praktijkWijzigenPopupPanel.setBorder(BorderFactory.createEmptyBorder(
				15, 25, 15, 25));

		JLabel lblFysiotherapeut = new JLabel("Naam");

		JLabel lblFa = new JLabel("Adres");

		JLabel lblPostcode = new JLabel("Postcode");

		JLabel lblPlaats = new JLabel("Plaats");

		JLabel lblTelefoonnummer = new JLabel("Telefoonnummer");

		JLabel lblEmail = new JLabel("E-mail");

		JLabel lblKvkNummer = new JLabel("KVK nummer");

		JLabel lblBtwNummer = new JLabel("BTW Nummer");

		ImmutablePraktijk praktijk = manager.getPraktijk();

		naam = new JTextField();
		naam.setColumns(10);

		adres = new JTextField();
		adres.setColumns(10);

		postcode = new JTextField();
		postcode.setColumns(10);

		plaats = new JTextField();
		plaats.setColumns(10);

		telefoonnummer = new JTextField();
		telefoonnummer.setColumns(10);

		email = new JTextField();
		email.setColumns(10);

		kvkNummer = new JTextField();
		kvkNummer.setColumns(10);

		btwNummer = new JTextField();
		btwNummer.setColumns(10);

		if (praktijk != null) {
			naam.setText(praktijk.getNaam());
			adres.setText(praktijk.getAdres());
			postcode.setText(praktijk.getPostcode());
			plaats.setText(praktijk.getPlaats());
			telefoonnummer.setText(praktijk.getTelefoonnummer());
			email.setText(praktijk.getEmail());
			kvkNummer.setText("" + praktijk.getKvk());
			btwNummer.setText(praktijk.getBtwNummer());
		}

		btnToevoegen = new JButton("wijzigen");
		btnAnnuleren = new JButton("annuleren");

		btnToevoegen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				 ArrayList<String> fouten = wijzigPrakijk();
				 if (!fouten.isEmpty()) {
				 String foutmeldingen = "";
				 for (int i = 0; i < fouten.size(); i++) {
				 foutmeldingen = foutmeldingen + fouten.get(i) + " \n";
				 }
				
				 JOptionPane.showMessageDialog(null, foutmeldingen,
				 "Het volgende ging fout: ",
				 JOptionPane.PLAIN_MESSAGE);
				 } else {
				 cardLayout.show(cardPanel, "praktijkGui");
				 }

			}
		});
		btnAnnuleren.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cardLayout.show(cardPanel, "praktijkGui");
			}
		});

		GroupLayout groupLayout = new GroupLayout(praktijkWijzigenPopupPanel);
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblTelefoonnummer)
																						.addComponent(
																								lblPlaats)
																						.addComponent(
																								lblPostcode)
																						.addComponent(
																								lblFa)
																						.addComponent(
																								lblFysiotherapeut)
																						.addComponent(
																								lblEmail)
																						.addComponent(
																								lblKvkNummer)
																						.addComponent(
																								lblBtwNummer)
																						.addComponent(
																								btnAnnuleren))
																		.addGap(18)
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								groupLayout
																										.createSequentialGroup()
																										.addComponent(
																												btnToevoegen))
																						.addComponent(
																								plaats,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								adres,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								naam,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								telefoonnummer,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								btwNummer,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								kvkNummer,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								email,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								postcode,
																								GroupLayout.PREFERRED_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.PREFERRED_SIZE))))
										.addContainerGap(304, Short.MAX_VALUE)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblFysiotherapeut)
														.addComponent(
																naam,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblFa)
														.addComponent(
																adres,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblPostcode)
														.addComponent(
																postcode,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblPlaats)
														.addComponent(
																plaats,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblTelefoonnummer)
														.addComponent(
																telefoonnummer,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblEmail)
														.addComponent(
																email,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblKvkNummer)
														.addComponent(
																kvkNummer,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblBtwNummer)
														.addComponent(
																btwNummer,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(31)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																btnAnnuleren)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				btnToevoegen)))
										.addContainerGap(60, Short.MAX_VALUE)));

		praktijkWijzigenPopupPanel.setLayout(groupLayout);
		defaultPanel.add(praktijkWijzigenPopupPanel);
		return defaultPanel;

	}
	
	public ArrayList<String> wijzigPrakijk() {		
		
		String tNaam = naam.getText();
		String tAdres = adres.getText();
		String tPostcode = postcode.getText();
		String tPlaats = plaats.getText();
		String tTelefoonnummer = telefoonnummer.getText();
		String tEmail = email.getText();
		String tKvkNummer = kvkNummer.getText();
		String tBtwNummer = btwNummer.getText();

		ArrayList<String> fouten = new ArrayList<String>();
/*
		if (!tNaam.matches("[a-zA-Z\\s]") || tNaam.isEmpty()
				|| tNaam.length() > 100) {
			String foutmelding = "De voornaam is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (tAdres.isEmpty() || tAdres.length() > 100) {
			String foutmelding = "Het adres is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (!tPostcode.matches("[a-zA-Z0-9\\s]") || tPostcode.isEmpty()
				|| tNaam.length() > 100) {
			String foutmelding = "De postcode is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (!tPlaats.matches("[a-zA-Z\\s]") || tPlaats.isEmpty()
				|| tPlaats.length() > 100) {
			String foutmelding = "De plaats is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (tTelefoonnummer.matches("[a-zA-Z\\s]") || tTelefoonnummer.isEmpty()
				|| tTelefoonnummer.length() > 100) {
			String foutmelding = "Het telefoonnummer is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (tEmail.isEmpty() || tEmail.length() > 100) {
			String foutmelding = "De email is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (!tKvkNummer.matches("[a-zA-Z0-9\\s]")
				|| tKvkNummer.isEmpty() || tKvkNummer.length() > 100) {
			String foutmelding = "Het kvknummer is niet correct ingevuld.";
			fouten.add(foutmelding);
		}

		if (!tBtwNummer.matches("[a-zA-Z0-9\\s]")
				|| tBtwNummer.isEmpty() || tBtwNummer.length() > 100) {
			String foutmelding = "Het btwnummer is niet correct ingevuld.";
			fouten.add(foutmelding);
		}
		*/
		if (fouten.size() == 0) {

			int KvkNummer = Integer.parseInt(tKvkNummer);
			
			manager.updatePraktijk(tNaam, tAdres, tPostcode,
					tPlaats, tTelefoonnummer, tEmail, KvkNummer,
					tBtwNummer);
		}
		
		return fouten;
	}
}