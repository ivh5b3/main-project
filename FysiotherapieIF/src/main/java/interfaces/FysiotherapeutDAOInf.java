package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import businessEntityDomain.Fysiotherapeut;

public interface FysiotherapeutDAOInf extends Remote  {
	
	public static final String servicename = "fysiotherapeutDAO";

	public boolean insert(Fysiotherapeut fysiotherapeut) throws RemoteException;

	public boolean delete(Fysiotherapeut fysiotherapeut) throws RemoteException;

	public boolean update(Fysiotherapeut fysiotherapeut) throws RemoteException;

	public Fysiotherapeut get(String bsn) throws RemoteException;

	public ArrayList<Fysiotherapeut> getAll() throws RemoteException;
}
