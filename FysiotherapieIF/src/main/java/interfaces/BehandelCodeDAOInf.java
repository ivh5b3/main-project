/**
 * 
 */
package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import businessEntityDomain.BehandelCode;

/**
 * @author robbie
 *
 */
public interface BehandelCodeDAOInf extends Remote {
	
	public static final String servicename = "behandelCodeDAO";
	
	public ArrayList<BehandelCode> getAll() throws RemoteException;
	
	public BehandelCode get(String behandelCode) throws RemoteException;
}
