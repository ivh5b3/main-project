package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import businessEntityDomain.BehandelTraject;
import businessEntityDomain.FilterInf;
import businessEntityDomain.ImmutableBehandelTraject;
import businessEntityDomain.ImmutableSessie;

/**
 * @author Sander van Belleghem
 *
 */
public interface BehandelTrajectDAOInf extends Remote  {
	
	public static final String servicename = "behandelTrajectDAO";

	public boolean insert(BehandelTraject behandelTraject) throws RemoteException;

	public boolean delete(BehandelTraject behandelTraject) throws RemoteException;

	public BehandelTraject get(int id) throws RemoteException;
	
	public ArrayList<BehandelTraject> getBehandelingen(FilterInf[] filters) throws RemoteException;

	public ArrayList<BehandelTraject> getAll() throws RemoteException;
	
	public boolean heeftBehandelingen(FilterInf[] filters) throws RemoteException;
	
	public boolean isMogelijk(ImmutableBehandelTraject behandelTraject, ImmutableSessie sessie) throws RemoteException;
}
