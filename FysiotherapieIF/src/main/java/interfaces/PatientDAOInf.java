/**
 * 
 */
package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import businessEntityDomain.ImmutablePatient;

/**
 * @author robbie
 *
 */
public interface PatientDAOInf extends Remote {
	
	public static final String servicename = "patientDAO";
	
	public ArrayList<ImmutablePatient> getAll() throws RemoteException;
	
	public ImmutablePatient getByBSN(String bsn) throws RemoteException;
}
