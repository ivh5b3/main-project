package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import businessEntityDomain.Praktijk;

public interface PraktijkDAOInf extends Remote  {
	
	public static final String servicename = "praktijkDAO";

	public boolean insert(Praktijk fysioPraktijk) throws RemoteException;

	public boolean update(Praktijk fysioPraktijk) throws RemoteException;

	public Praktijk get() throws RemoteException;
}
