/**
 * 
 */
package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import businessEntityDomain.FilterInf;
import businessEntityDomain.Sessie;

/**
 * @author robbie
 *
 */
public interface SessieDAOInf extends Remote  {
	
	public static final String servicename = "sessieDAO";

	public boolean insert(Sessie sessie) throws RemoteException;

	public boolean delete(Sessie sessie) throws RemoteException;

	public boolean update(Sessie sessie) throws RemoteException;

	public Sessie get(int id) throws RemoteException;

	public ArrayList<Sessie> get(FilterInf[] filters) throws RemoteException;

	public ArrayList<Sessie> getByBehandelTraject(int behandelTrajectId) throws RemoteException;
}
