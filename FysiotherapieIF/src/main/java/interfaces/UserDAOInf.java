/**
 * 
 */
package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import businessEntityDomain.User;


/**
 * @author Sander van Belleghem
 *
 */
public interface UserDAOInf extends Remote  {
	
	public static final String servicename = "userDAO";
	
	public boolean login(String username, String password) throws RemoteException;
	
	public User getUser(String username, String password) throws RemoteException;
	
	public boolean isLeidinggevende(String username) throws RemoteException;
}
