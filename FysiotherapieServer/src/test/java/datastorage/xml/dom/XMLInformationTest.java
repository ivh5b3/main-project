/**
 * 
 */
package datastorage.xml.dom;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;

import org.junit.Test;

/**
 * @author robbie
 *
 */
public class XMLInformationTest {


	@Test
	public void elementsArrayToMap() {
		String[] elements = new String[] {
				"lower_case",
				"upperCase",
				"Super_test",
				"SuperTest"
		};
		
		LinkedHashMap<String, String> result = XMLInformation.elementsArrayToMap(elements);
		
		LinkedHashMap<String, String> expectedResult = new LinkedHashMap<String, String>();
		
		expectedResult.put("lower_case", "lower_case");
		expectedResult.put("upperCase", "upper_case");
		expectedResult.put("Super_test", "super_test");
		expectedResult.put("SuperTest", "super_test");

		assertEquals(expectedResult, result);
	}

}
