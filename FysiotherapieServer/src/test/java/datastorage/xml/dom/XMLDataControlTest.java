/**
 * 
 */
package datastorage.xml.dom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

/**
 * @author robbie
 *
 */
public class XMLDataControlTest {

	private XMLDataControl dataControl;
	private ArrayList<HashMap<String, String>> existingValues;
	private HashMap<String, String> nonExistingValues;
	private String nonExistingIdentifier;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		XMLInformation info = new XMLInformation(
				XMLInformation.elementsArrayToMap(new String[] { "naam",
						"adres", "btwNummer" }), "testen", "test");
		
		info.setPathToXMLFiles("./resources/xml/testfiles/");

		XMLDOMDocument doc = new XMLDOMDocument(info.getXMLFileName(),
				info.getXMLSchemaName());

		this.dataControl = new XMLDataControl(doc.getDocument(), info);
		
		nonExistingValues = new HashMap<String, String>();
		
		nonExistingValues.put("naam", "Piet");
		nonExistingValues.put("adres", "Hogeschoollaan 1");
		nonExistingValues.put("btwNummer", "NL001234567B01");
		
		nonExistingIdentifier = "b303";
		
		existingValues = new ArrayList<HashMap<String, String>>();
		
		HashMap<String, String> map1 = new HashMap<String, String>();
		
		map1.put("naam", "Jan");
		map1.put("adres", "Koningin Beatrixstraat 17");
		map1.put("btwNummer", "NL007654321B01");	
		map1.put("id", "1");
		
		HashMap<String, String> map2 = new HashMap<String, String>();
		
		map2.put("naam", "Kees");
		map2.put("adres", "Koningin Julianastraat 26");
		map2.put("btwNummer", "NL007694321B01");	
		map2.put("id", "2");
		
		existingValues.add(map1);
		existingValues.add(map2);
	}

	@Test
	public void insertWithIncrementingId() {		
		assertTrue(dataControl.insert(nonExistingValues, true));
	}
	
	@Test
	public void deleteExistingElement() {		
		assertTrue(dataControl.delete(existingValues.get(0)));
	}
	
	@Test
	public void deleteNonExistingElement() {		
		nonExistingValues.put("id", nonExistingIdentifier);
		
		assertFalse(dataControl.delete(nonExistingValues));
	}	
	
	@Test(expected=IllegalArgumentException.class)
	public void deleteElementWithoutIdentifier() {		
		dataControl.delete(nonExistingValues);
	}
	
	@Test
	public void updateExistingElement() {		
		assertTrue(dataControl.update(existingValues.get(0)));
	}
	
	@Test
	public void updateNonExistingElement() {		
		nonExistingValues.put("id", nonExistingIdentifier);
		
		assertFalse(dataControl.update(nonExistingValues));
	}	
	
	@Test(expected=IllegalArgumentException.class)
	public void updateElementWithoutIdentifier() {		
		dataControl.update(nonExistingValues);
	}
	
	@Test
	public void getExistingElementByIdentifier() {		
		assertEquals(existingValues.get(0), dataControl.getByIdentifier(existingValues.get(0).get("id")));
	}
	
	@Test
	public void getNonExistingElementByIdentifier() {		
		assertNull(dataControl.getByIdentifier(nonExistingIdentifier));
	}
	
	@Test
	public void getWhereElementsExistingValue() {
		HashMap<String, String> elements = new HashMap<String, String> ();
		
		elements.put("naam", existingValues.get(0).get("naam"));
		
		ArrayList<HashMap<String, String>> result = dataControl.getWhereElements(elements);
		
		assertEquals(existingValues.get(0), result.get(0));
	}

	@Test
	public void getWhereElementsNonExistingValue() {
		HashMap<String, String> elements = new HashMap<String, String> ();
		
		elements.put("naam", "piet");
		
		ArrayList<HashMap<String, String>> result = dataControl.getWhereElements(elements);
		
		assertTrue(result.isEmpty());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getWhereElementsNonExistingElement() {
		HashMap<String, String> elements = new HashMap<String, String> ();
		
		elements.put("dezebestaatniet", existingValues.get(0).get("naam"));
		
		dataControl.getWhereElements(elements);
	}	
	
	@Test
	public void getAll() {		
		assertEquals(existingValues, dataControl.getAll());
	}	
	
	@Test
	public void getLastIdentifierValue() {		
		assertEquals(existingValues.get(existingValues.size() - 1).get("id"), dataControl.getLastIdentifierValue());
	}		
}
