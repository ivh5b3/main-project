package main;

import interfaces.BehandelCodeDAOInf;
import interfaces.BehandelTrajectDAOInf;
import interfaces.FysiotherapeutDAOInf;
import interfaces.FysiotherapieInf;
import interfaces.PatientDAOInf;
import interfaces.PraktijkDAOInf;
import interfaces.SessieDAOInf;
import interfaces.UserDAOInf;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import rmi.FysiotherapieApiImpl;
import util.Settings;
import datastorage.xml.dom.XMLDOMBehandelCodeDAO;
import datastorage.xml.dom.XMLDOMBehandelTrajectDAO;
import datastorage.xml.dom.XMLDOMFysiotherapeutDAO;
import datastorage.xml.dom.XMLDOMPatientDAO;
import datastorage.xml.dom.XMLDOMPraktijkDAO;
import datastorage.xml.dom.XMLDOMSessieDAO;
import datastorage.xml.dom.XMLDOMUserDAO;

public class Server {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(Server.class);

	public Server() throws RemoteException {
		logger.info("Constructor");
		logger.info("Done");
	}

	public static void main(String args[]) {

		Properties properties = new Settings();

		String hostname = null;

		if (args.length != 1) {
			System.out.println("Usage: Server [hostname]");
			System.exit(1);
		} else {
			hostname = args[0];
		}

		// Configure logging.
		PropertyConfigurator.configure("./resources/"
				+ properties.getProperty("logconfig"));

		logger.info("Starting application ---------------------------------");

		System.setProperty("java.rmi.server.hostname", hostname);
		System.setProperty("java.rmi.server.codebase", "http://" + hostname
				+ "/classes/bin/");
		System.setProperty("java.security.policy", "http://" + hostname
				+ "/classes/resources/" + properties.getProperty("policy"));

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
			logger.info("SecurityManager installed");
		}

		try {
			// BehandelTrajectDAO
			logger.info("Creating behandeltraject stub");
			XMLDOMBehandelTrajectDAO behandelTrajectDAO = new XMLDOMBehandelTrajectDAO();
			BehandelTrajectDAOInf stub = (BehandelTrajectDAOInf) UnicastRemoteObject
					.exportObject(behandelTrajectDAO, 0);

			logger.info("Locating registry on host '" + hostname + "'");
			Registry registry = LocateRegistry.getRegistry(hostname);

			logger.info("Trying to register stub using name '"
					+ BehandelTrajectDAOInf.servicename + "'");
			registry.rebind(BehandelTrajectDAOInf.servicename, stub);
			logger.info("Stub registered");

			// FysiotherapeutDAO
			logger.info("Creating behandeltraject stub");
			XMLDOMFysiotherapeutDAO fysiotherapeutDAO = new XMLDOMFysiotherapeutDAO();
			FysiotherapeutDAOInf fysiotherapeutDAOStub = (FysiotherapeutDAOInf) UnicastRemoteObject
					.exportObject(fysiotherapeutDAO, 0);

			logger.info("Locating registry on host '" + hostname + "'");
			Registry fysiotherapeutDAORegistry = LocateRegistry
					.getRegistry(hostname);

			logger.info("Trying to register stub using name '"
					+ FysiotherapeutDAOInf.servicename + "'");
			fysiotherapeutDAORegistry.rebind(FysiotherapeutDAOInf.servicename,
					fysiotherapeutDAOStub);
			logger.info("Stub registered");

			// FysiotherapiePraktijkDAO
			logger.info("Creating behandeltraject stub");
			XMLDOMPraktijkDAO fysiotherapiePraktijkDAO = new XMLDOMPraktijkDAO();
			PraktijkDAOInf fysiotherapiePraktijkDAOStub = (PraktijkDAOInf) UnicastRemoteObject
					.exportObject(fysiotherapiePraktijkDAO, 0);

			logger.info("Locating registry on host '" + hostname + "'");
			Registry fysiotherapiePraktijkDAORegistry = LocateRegistry
					.getRegistry(hostname);

			logger.info("Trying to register stub using name '"
					+ PraktijkDAOInf.servicename + "'");
			fysiotherapiePraktijkDAORegistry.rebind(PraktijkDAOInf.servicename,
					fysiotherapiePraktijkDAOStub);
			logger.info("Stub registered");

			// SessieDAO
			logger.info("Creating SessieDAO stub");
			XMLDOMSessieDAO sessieDAO = new XMLDOMSessieDAO();
			SessieDAOInf sessieDAOStub = (SessieDAOInf) UnicastRemoteObject
					.exportObject(sessieDAO, 0);

			logger.info("Locating registry on host '" + hostname + "'");
			Registry sessieDAORegistry = LocateRegistry.getRegistry(hostname);

			logger.info("Trying to register stub using name '"
					+ SessieDAOInf.servicename + "'");
			sessieDAORegistry.rebind(SessieDAOInf.servicename, sessieDAOStub);
			logger.info("Stub registered");

			// UserDAO
			logger.info("Creating UserDAO stub");
			XMLDOMUserDAO userDAO = new XMLDOMUserDAO();
			UserDAOInf userDAOStub = (UserDAOInf) UnicastRemoteObject
					.exportObject(userDAO, 0);

			logger.info("Locating registry on host '" + hostname + "'");
			Registry userDAORegistry = LocateRegistry.getRegistry(hostname);

			logger.info("Trying to register stub using name '"
					+ UserDAOInf.servicename + "'");
			userDAORegistry.rebind(UserDAOInf.servicename, userDAOStub);
			logger.info("Stub registered");

			// BehandelCodeDAO
			logger.info("Creating BehandelCodeDAO stub");

			XMLDOMBehandelCodeDAO behandelCodeDAO = new XMLDOMBehandelCodeDAO();
			BehandelCodeDAOInf behandelCodeDAOStub = (BehandelCodeDAOInf) UnicastRemoteObject
					.exportObject(behandelCodeDAO, 0);

			logger.info("Locating registry on host '" + hostname + "'");
			Registry behandelCodeDAORegistry = LocateRegistry
					.getRegistry(hostname);

			logger.info("Trying to register stub using name '"
					+ BehandelCodeDAOInf.servicename + "'");
			behandelCodeDAORegistry.rebind(BehandelCodeDAOInf.servicename,
					behandelCodeDAOStub);

			logger.info("Stub registered");

			// PatientDAO
			logger.info("Creating PatientDAO stub");

			XMLDOMPatientDAO patientDAO = new XMLDOMPatientDAO();
			PatientDAOInf patientDAOStub = (PatientDAOInf) UnicastRemoteObject
					.exportObject(patientDAO, 0);

			logger.info("Locating registry on host '" + hostname + "'");
			Registry patientDAORegistry = LocateRegistry.getRegistry(hostname);

			logger.info("Trying to register stub using name '"
					+ PatientDAOInf.servicename + "'");
			patientDAORegistry
					.rebind(PatientDAOInf.servicename, patientDAOStub);

			logger.info("Stub registered");

			// FysiotherapieApi
			logger.info("Creating FysiotherpieApi stub");

			FysiotherapieApiImpl fysiotherapieApiIml = new FysiotherapieApiImpl();
			FysiotherapieInf fysiotherapieApiStub = (FysiotherapieInf) UnicastRemoteObject
					.exportObject(fysiotherapieApiIml, 0);

			logger.info("Locating registry on host '" + hostname + "'");
			Registry fysiotherapieApiRegistry = LocateRegistry
					.getRegistry(hostname);

			logger.info("Trying to register stub using name '"
					+ FysiotherapieInf.servicename + "'");
			fysiotherapieApiRegistry.rebind(FysiotherapieInf.servicename,
					fysiotherapieApiStub);

			logger.info("Stub registered");

			logger.info("Server ready");

		} catch (java.rmi.ConnectException e) {
			logger.error("Could not connect: " + e.getMessage());
		} catch (java.security.AccessControlException e) {
			logger.error("Could not access registry: " + e.getMessage());
		} catch (Exception e) {
			logger.error("Server exception: " + e.toString());
			// e.printStackTrace();
		}
	}
}
