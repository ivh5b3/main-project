/**
 * 
 */
package Systeem.rmi.business;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import Systeem.rmi.domain.RmiKlantIRead;



/**
 * @author Gregor
 *
 */
public interface RemoteKlantenInterface extends Remote {
	
	public static final String servicename = "facturatieServer";
	
	public RmiKlantIRead geefKlant(String bsn) throws RemoteException;

	public List<RmiKlantIRead> geefAlleKlanten() throws RemoteException;
}
