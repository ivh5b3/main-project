/**
 * 
 */
package datastorage.xml.dom;

import interfaces.UserDAOInf;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import businessEntityDomain.ImmutableUser;
import businessEntityDomain.User;

/**
 * @author Sander van Belleghem
 *
 */
public class XMLDOMUserDAO extends XMLDOMDAO<User> implements UserDAOInf {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(XMLDOMUserDAO.class);

	public XMLDOMUserDAO() {
		super(new XMLInformation(
				XMLInformation.elementsArrayToMap(new String[] { "username",
						"password", "functie" }), "users", "user"));
	}

	@Override
	public boolean login(String username, String password) {
		ArrayList<User> users = getAll();

		try {
			logger.debug("Trying to login with username:" + username
					+ " and password:" + password);
			for (ImmutableUser user : users) {
				if (username.equals(user.getUsername())) {
					if (password.equals(user.getPassword())) {
						logger.debug("Successfully logged in.");

						return true;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Something went wrong:" + e.getMessage());
		}

		logger.debug("Not logged in.");
		return false;

	}

	public User getUser(String username, String password) {
		
		HashMap<String, String> values = new HashMap<String, String>();

		values.put("username", username);
		values.put("password", password);

		ArrayList<User> users = getWhereElements(values);

		if (!users.isEmpty()) {
			return users.get(0);
		}

		return null;
	}

	@Override
	protected HashMap<String, String> createValues(User user, boolean insert) {

		HashMap<String, String> values = new HashMap<String, String>();

		values.put("username", user.getUsername());
		values.put("password", user.getPassword());
		values.put("functie", user.getFunctie());

		return values;
	}

	@Override
	protected User createInstanceFromValues(HashMap<String, String> values) {
		if (values != null) {
			User user = new User(values.get("username"),
					values.get("password"), values.get("functie"));

			user.setUserNumber(values.get("id"));

			return user;
		}
		return null;
	}

	@Override
	public boolean isLeidinggevende(String username) throws RemoteException {
		HashMap<String, String> values = new HashMap<String, String>();

		values.put("username", username);

		ArrayList<User> users = getWhereElements(values);

		if (!users.isEmpty()) {
			return users.get(0).getFunctie().equals("leidinggevende");
		}

		throw new IllegalArgumentException("Username doesn't exists in database");
	}
}