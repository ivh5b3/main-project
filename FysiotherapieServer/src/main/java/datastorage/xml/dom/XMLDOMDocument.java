package datastorage.xml.dom;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XMLDOMDocument {

	static Logger logger = Logger.getLogger(XMLDOMDocument.class);

	private String xmlFilename;
	private String xmlSchema;
	private Document document = null;

	public XMLDOMDocument(String xmlFilename, String xmlSchema) {

		logger.debug("Xmlfilename: " + xmlFilename + " en xmlSchema: " + xmlSchema);
		this.xmlFilename = xmlFilename;
		this.xmlSchema = xmlSchema;
	}

	public Document getDocument() {

		if (document == null) {

			Schema schema = getValidationSchema();

			logger.debug("Haal xml document op: " + xmlFilename);
			if (schema == null) {
				logger.error("Schema file not found or contains errors, XML file not validated!");
			} else {
				validateDocument(xmlFilename, schema);
			}

			// Whether validated or not, try to build the
			// Document Object Model from the inputfile
			document = buildDocument(xmlFilename);

			if (document == null) {
				logger.fatal("No XML data source found! Cannot read application data!");
				// Again, here we could decide to cancel initialization of the
				// application.
				// A non-validated inputfile could lead to read-errors when
				// reading domain
				// objects from the data source. For now, we continue. Fingers
				// crossed.
			}
		}

		return document;
	}

	public void writeDocument() {

		logger.debug("Writing XML document to file " + xmlFilename);

		try {			
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			DOMSource source = new DOMSource(document);
			
			StreamResult result = new StreamResult(new File(xmlFilename));

			transformer.transform(source, result);
			
			logger.debug("done writing file");
		} catch (TransformerConfigurationException e) {
			logger.error(e.getMessage());
		} catch (TransformerException e) {
			logger.error(e.getMessage());
		}
	}

	private Document buildDocument(String filename) {

		DocumentBuilderFactory builderFactory = DocumentBuilderFactory
				.newInstance();

		DocumentBuilder builder = null;

		try {
			builder = builderFactory.newDocumentBuilder();

			File file = new File(filename);

			if (file.exists()) {
				document = builder.parse(new FileInputStream(file));
			} else {
				System.out.println("Could not read file " + filename);
			}
		} catch (ParserConfigurationException e) {
			System.out.println(e.getMessage());
		} catch (SAXException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		return document;
	}

	private Schema getValidationSchema() {
		Schema schema = null;

		try {

			String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;

			SchemaFactory factory = SchemaFactory.newInstance(language);

			File xmlSchemaFile = new File(xmlSchema);

			if (xmlSchemaFile.exists()) {
				schema = factory.newSchema(xmlSchemaFile);

				System.out.println("Schema created");
			} else {
				System.out.println("Schemafile does not exist.");
			}
		} catch (Exception e) {
			System.out.println("Error reading schema file: " + e.getMessage());
		}

		return schema;
	}

	private boolean validateDocument(String xmlFile, Schema schema) {

		boolean result = false;

		try {
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(xmlFile));

			result = true;

			System.out.println("Valid XML: " + xmlFile);
		} catch (IOException e) {
			System.out.println("I/O error: " + e.getMessage());
		} catch (SAXException e) {
			System.out.println("Parse exception: " + e.getMessage());
		}

		return result;
	}
}
