/**
 * 
 */
package datastorage.xml.dom;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import businessEntityDomain.Praktijk;
import businessEntityDomain.Sessie;
import interfaces.PraktijkDAOInf;

/**
 * @author Sander van Belleghem
 *
 */
public class XMLDOMPraktijkDAO extends XMLDOMDAO<Praktijk> implements
		PraktijkDAOInf {

	public XMLDOMPraktijkDAO() {
		super(new XMLInformation(
				XMLInformation.elementsArrayToMap(new String[] { "naam",
						"adres", "postcode", "plaats", "telefoonnummer",
						"email", "kvk", "btwNummer" }), "praktijken",
				"praktijk"));
	}

	@Override
	public Praktijk get() {
		ArrayList<Praktijk> praktijken = getAll();
System.out.println(praktijken.size());
		if (praktijken.size() > 0) {
			return praktijken.get(0);
		}
		
		return null;
	}

	@Override
	protected HashMap<String, String> createValues(Praktijk object,
			boolean insert) {

		HashMap<String, String> values = new HashMap<String, String>();

		values.put("naam", object.getNaam());
		values.put("adres", object.getAdres());
		values.put("postcode", object.getPostcode());
		values.put("plaats", object.getPlaats());
		values.put("telefoonnummer", object.getTelefoonnummer());
		values.put("email", object.getEmail());
		values.put("kvk", String.valueOf(object.getKvk()));
		values.put("btwNummer", object.getBtwNummer());
		values.put("id", "1");

		return values;
	}

	@Override
	protected Praktijk createInstanceFromValues(HashMap<String, String> values) {
		if (values != null) {

			return new Praktijk(values.get("naam"), values.get("adres"),
					values.get("postcode"), values.get("plaats"),
					values.get("telefoonnummer"), values.get("email"),
					Integer.parseInt(values.get("kvk")),
					values.get("btwNummer"));
		}

		return null;
	}
}