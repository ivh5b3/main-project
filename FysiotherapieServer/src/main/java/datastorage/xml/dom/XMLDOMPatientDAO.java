/**
 * 
 */
package datastorage.xml.dom;

import interfaces.PatientDAOInf;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;

import rmi.RmiConnection;
import util.Settings;
import Systeem.rmi.business.RemoteKlantenInterface;
import Systeem.rmi.domain.RmiKlantIRead;
import businessEntityDomain.ImmutablePatient;
import businessEntityDomain.Patient;

/**
 * @author robbie
 *
 */
public class XMLDOMPatientDAO implements PatientDAOInf {
	
	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(XMLDOMPatientDAO.class);

	private RmiConnection connection;

	public XMLDOMPatientDAO() {

		try {
			Properties properties = new Settings();

			connection = new RmiConnection(
					properties.getProperty("facturatie_hostname"));

		} catch (Exception e) {
			logger.debug("Exception while loading properties: " + e.getMessage());
		}
	}

	@Override
	public ArrayList<ImmutablePatient> getAll() {
		RemoteKlantenInterface remoteKlanten = (RemoteKlantenInterface) connection
				.getService(RemoteKlantenInterface.servicename);
		
		ArrayList<ImmutablePatient> patienten = new ArrayList<ImmutablePatient>();
		
		try {
			for (RmiKlantIRead klant : remoteKlanten.geefAlleKlanten()) {
				patienten.add(klant);
			}
		} catch (RemoteException e) {
			logger.debug("RemoteException: " + e.getMessage());
		}
		
		logger.debug("Aantal patienten opgehaald: " + patienten.size());
		
		return patienten;
	}

	@Override
	public ImmutablePatient getByBSN(String bsn) {
		
		String voornaam = "Patient";
		String achternaam = "van Fysio";
		String geslacht = "vrouw";
		String adres = "kiplaan 20";
		String postcode = "4929AA";
		String plaats = "Breda";
		String telefoonnummer = "06-12345672";
		String email = "patientvanfysio@gmail.com";

		return new Patient(bsn, voornaam, achternaam, geslacht, adres,
				postcode, plaats, telefoonnummer, email);
		
		/**
		RemoteKlantenInterface remoteKlanten = (RemoteKlantenInterface) connection
				.getService(RemoteKlantenInterface.servicename);

		try {
			return remoteKlanten.geefKlant(bsn);
		} catch (RemoteException e) {
			logger.debug("RemoteException: " + e.getMessage());
			
			return null;
		}
		*/
	}
}
