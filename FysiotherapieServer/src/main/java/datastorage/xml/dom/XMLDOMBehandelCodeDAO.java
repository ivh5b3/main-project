/**
 * 
 */
package datastorage.xml.dom;

import interfaces.BehandelCodeDAOInf;

import java.util.HashMap;

import org.apache.log4j.Logger;

import businessEntityDomain.BehandelCode;

/**
 * @author robbie
 *
 */
public class XMLDOMBehandelCodeDAO extends XMLDOMDAO<BehandelCode> implements
		BehandelCodeDAOInf {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(XMLDOMBehandelCodeDAO.class);

	public XMLDOMBehandelCodeDAO() {
		super(new XMLInformation(
				XMLInformation.elementsArrayToMap(new String[] {
						"behandelingnaam", "aantalsessies", "sessieduur",
						"tariefbehandeling" }), "behandelcodes", "behandeling",
				"behandelcode", "last_behandelcode"));
	}

	@Override
	protected HashMap<String, String> createValues(BehandelCode object,
			boolean insert) {
		// Er zullen geen behandelcodes worden ingevoerd of worden gewijzigd
		return null;
	}

	@Override
	protected BehandelCode createInstanceFromValues(
			HashMap<String, String> values) {
		if (values != null) {

			double sessieduur = Double.parseDouble(values.get("sessieduur"));
			double tariefbehandeling = Double.parseDouble(values
					.get("tariefbehandeling"));

			return new BehandelCode(values.get("behandelcode"),
					values.get("behandelingnaam"), Integer.parseInt(values
							.get("aantalsessies")), sessieduur,
					tariefbehandeling);
		}

		return null;
	}

}
