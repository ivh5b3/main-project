/**
 * 
 */
package datastorage.xml.dom;

import interfaces.SessieDAOInf;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import businessEntityDomain.FilterInf;
import businessEntityDomain.Sessie;

/**
 * @author robbie
 *
 */
public class XMLDOMSessieDAO extends XMLDOMDAO<Sessie> implements SessieDAOInf {

	public XMLDOMSessieDAO() {
		super(new XMLInformation(
				XMLInformation.elementsArrayToMap(new String[] {
						"beginDatumTijd", "eindDatumTijd", "behandelTrajectId",
						"status", "opmerking" }), "sessies", "sessie"));
	}

	@Override
	public ArrayList<Sessie> get(FilterInf[] filters) {
		ArrayList<Sessie> sessies = getAll();

		Iterator<Sessie> sessieIterator = sessies.iterator();

		while (sessieIterator.hasNext()) {
			Sessie sessie = sessieIterator.next();

			for (FilterInf filter : filters) {
				if (!filter.checkConstraints(sessie)) {
					sessieIterator.remove();

					// De overige filters hoeven niet meer gecontroleerd te
					// worden
					break;
				}
			}
		}

		return sessies;
	}

	@Override
	public ArrayList<Sessie> getByBehandelTraject(int behandelTrajectId) {
		HashMap<String, String> map = new HashMap<String, String>();

		map.put("behandel_traject_id", String.valueOf(behandelTrajectId));

		return getWhereElements(map);
	}

	@Override
	protected HashMap<String, String> createValues(Sessie sessie, boolean insert) {
		if (insert) {
			sessie.setSessieNummer(Integer.parseInt(dataControl
					.getLastIdentifierValue()) + 1);
		}

		HashMap<String, String> values = new HashMap<String, String>();

		values.put("beginDatumTijd",
				String.valueOf(sessie.getBeginDatumTijd().getTime()));
		values.put("eindDatumTijd",
				String.valueOf(sessie.getEindDatumTijd().getTime()));
		values.put("status", sessie.getStatus());
		values.put("behandelTrajectId",
				String.valueOf(sessie.getBehandelTrajectId()));
		values.put("id", String.valueOf(sessie.getSessieNummer()));
		values.put("opmerking", sessie.getOpmerking());

		return values;
	}

	@Override
	protected Sessie createInstanceFromValues(HashMap<String, String> values) {
		if (values != null) {

			Sessie sessie = new Sessie(new Timestamp(Long.parseLong(values
					.get("beginDatumTijd"))), new Timestamp(
					Long.parseLong(values.get("eindDatumTijd"))),
					values.get("status"), Integer.parseInt(values.get("behandelTrajectId")),
					values.get("opmerking"));

			sessie.setSessieNummer(Integer.parseInt(values.get("id")));

			return sessie;
		}

		return null;
	}
}