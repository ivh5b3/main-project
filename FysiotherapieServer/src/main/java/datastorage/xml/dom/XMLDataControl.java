/**
 * 
 */
package datastorage.xml.dom;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author robbie
 *
 */
public class XMLDataControl {

	static Logger logger = Logger.getLogger(XMLDataControl.class);

	private Document document;
	private XMLInformation info;

	public XMLDataControl(Document document, XMLInformation info) {
		if (document == null) {
			throw new IllegalArgumentException("Document cann't be null");
		}
		
		this.document = document;
		this.info = info;
	}

	public boolean insert(HashMap<String, String> values, boolean insert) {
		Node rootElement = document.getElementsByTagName(info.getRootName())
				.item(0);

		Element newChildElement = document.createElement(info.getRecordName());
		rootElement.appendChild(newChildElement);

		logger.debug("Zet attribute " + info.getIdentifierName() + ": "
				+ values.get(info.getIdentifierName()));
		newChildElement.setAttribute(info.getIdentifierName(),
				values.get(info.getIdentifierName()));

		for (String element : info.getElements()) {
			logger.debug("Maak element: " + element + " met waarde: "
					+ values.get(element));
			Element newElement = document.createElement(info
					.getXMLElementName(element));
			newElement
					.appendChild(document.createTextNode(values.get(element)));
			newChildElement.appendChild(newElement);
		}

		if (insert) {
			updateLastIdentifier(values);
		}

		return true;
	}

	public void updateLastIdentifier(HashMap<String, String> values) {
		// Update de laatste id waarde
		document.getElementsByTagName(info.getLastIdentifierElementName())
				.item(0).setTextContent(values.get(info.getIdentifierName()));
	}

	public boolean delete(HashMap<String, String> values) {
		if (values.containsKey(info.getIdentifierName())) {
			Node rootElement = document
					.getElementsByTagName(info.getRootName()).item(0);

			NodeList list = document.getElementsByTagName(info.getRecordName());

			for (int i = 0; i < list.getLength(); i++) {

				Node node = list.item(i);
				if (node instanceof Element) {
					Element child = (Element) node;

					if (values.get(info.getIdentifierName()).equals(
							child.getAttribute(info.getIdentifierName()))) {
						rootElement.removeChild(child);

						return true;
					}
				}
			}

			return false;
		}

		throw new IllegalArgumentException(
				"The values hashmap should contain the following key: "
						+ info.getIdentifierName());
	}

	public boolean update(HashMap<String, String> values) {
		if (values.containsKey(info.getIdentifierName())) {
			NodeList list = document.getElementsByTagName(info.getRecordName());

			for (int i = 0; i < list.getLength(); i++) {

				Node node = list.item(i);
				if (node instanceof Element) {
					Element child = (Element) node;

					if (values.get(info.getIdentifierName()).equals(
							child.getAttribute(info.getIdentifierName()))) {
						System.out.println("test2");
						for (String element : info.getElements()) {
							child.getElementsByTagName(
									info.getXMLElementName(element)).item(0)
									.setTextContent(values.get(element));
						}

						return true;
					}
				}
			}

			return false;
		}

		throw new IllegalArgumentException(
				"The values hashmap should contain the following key: "
						+ info.getIdentifierName());
	}

	public HashMap<String, String> getByIdentifier(String value) {
		logger.debug("getByIdentifier " + info.getIdentifierName() + " : "
				+ value);
		HashMap<String, String> map = new HashMap<String, String>();

		map.put(info.getIdentifierName(), value);

		return getWhereAttributesFirst(map);
	}

	public HashMap<String, String> getWhereAttributesFirst(
			HashMap<String, String> attributes) {
		NodeList list = document.getElementsByTagName(info.getRecordName());

		for (int i = 0; i < list.getLength(); i++) {

			Node node = list.item(i);
			if (node instanceof Element) {
				Element child = (Element) node;

				for (HashMap.Entry<String, String> entry : attributes
						.entrySet()) {
					if (child.getAttribute(entry.getKey()).equals(
							entry.getValue())) {
						logger.debug("Found first element with attributes");
						return createValuesMap(child);
					}
				}
			}
		}

		return null;
	}

	public ArrayList<HashMap<String, String>> getWhereAttributes(
			HashMap<String, String> attributes) {
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();

		NodeList list = document.getElementsByTagName(info.getRecordName());

		for (int i = 0; i < list.getLength(); i++) {

			Node node = list.item(i);
			if (node instanceof Element) {
				Element child = (Element) node;

				boolean voldoet = true;

				for (HashMap.Entry<String, String> entry : attributes
						.entrySet()) {
					if (!child.getAttribute(entry.getKey()).equals(
							entry.getValue())) {
						voldoet = false;
						// Als er een voorwaarde fout is hoeft er niet meer
						// verder te worden gecontroleerd.
						break;
					}
				}

				if (voldoet)
					arrayList.add(createValuesMap(child));
			}
		}

		return arrayList;
	}

	public ArrayList<HashMap<String, String>> getWhereElements(
			HashMap<String, String> wheres) {
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();

		NodeList list = document.getElementsByTagName(info.getRecordName());

		for (int i = 0; i < list.getLength(); i++) {

			Node node = list.item(i);
			if (node instanceof Element) {

				Element child = (Element) node;

				boolean voldoet = true;

				for (HashMap.Entry<String, String> entry : wheres.entrySet()) {
					NodeList childElements = child.getElementsByTagName(entry
							.getKey());

					if (childElements.getLength() == 1) {
						if (!child.getElementsByTagName(entry.getKey()).item(0)
								.getTextContent().equals(entry.getValue())) {
							voldoet = false;
							// Als er een voorwaarde fout is hoeft er niet meer
							// verder te worden gecontroleerd.
							break;
						}
					} else {
						throw new IllegalArgumentException(
								"The hashmap contains the element '"
										+ entry.getKey()
										+ "' but this element doesn't exists in the xml file");
					}
				}

				if (voldoet)
					arrayList.add(createValuesMap(child));
			}
		}

		return arrayList;
	}

	public ArrayList<HashMap<String, String>> getAll() {
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		logger.debug("Alles ophalen met element name: " + info.getRecordName());
		logger.debug(document);
		NodeList list = document.getElementsByTagName(info.getRecordName());

		for (int i = 0; i < list.getLength(); i++) {
			logger.debug("Object : " + i);

			Node node = list.item(i);
			if (node instanceof Element) {

				logger.debug("Add new object to arraylist");
				arrayList.add(createValuesMap((Element) node));
			}
		}

		return arrayList;
	}

	public String getLastIdentifierValue() {
		return document
				.getElementsByTagName(info.getLastIdentifierElementName())
				.item(0).getTextContent();
	}

	private HashMap<String, String> createValuesMap(Element child) {
		HashMap<String, String> map = new HashMap<String, String>();

		map.put(info.getIdentifierName(),
				child.getAttribute(info.getIdentifierName()));

		for (String element : info.getElements()) {
			map.put(element,
					child.getElementsByTagName(info.getXMLElementName(element))
							.item(0).getTextContent());

			logger.debug(element + ": " + info.getXMLElementName(element));
		}

		return map;
	}
}