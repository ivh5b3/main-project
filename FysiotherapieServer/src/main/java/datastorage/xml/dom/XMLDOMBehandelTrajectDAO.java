/**
 * 
 */
package datastorage.xml.dom;

import interfaces.BehandelCodeDAOInf;
import interfaces.BehandelTrajectDAOInf;
import interfaces.FysiotherapeutDAOInf;
import interfaces.PatientDAOInf;
import interfaces.SessieDAOInf;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import businessEntityDomain.BehandelCode;
import businessEntityDomain.BehandelTraject;
import businessEntityDomain.DatumFilter;
import businessEntityDomain.FilterInf;
import businessEntityDomain.Fysiotherapeut;
import businessEntityDomain.FysiotherapeutFilter;
import businessEntityDomain.ImmutableBehandelTraject;
import businessEntityDomain.ImmutableFysiotherapeut;
import businessEntityDomain.ImmutablePatient;
import businessEntityDomain.ImmutableSessie;
import businessEntityDomain.Sessie;

/**
 * @author robbie
 *
 */
public class XMLDOMBehandelTrajectDAO extends XMLDOMDAO<BehandelTraject>
		implements BehandelTrajectDAOInf {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(XMLDOMBehandelTrajectDAO.class);

	public XMLDOMBehandelTrajectDAO() {
		super(new XMLInformation(
				XMLInformation.elementsArrayToMap(new String[] {
						"behandelCode", "patientId", "fysiotherapeutId" }),
				"behandeltrajecten", "behandeltraject"));
	}

	@Override
	protected HashMap<String, String> createValues(BehandelTraject object,
			boolean insert) {

		if (insert) {
			object.setId(Integer.parseInt(dataControl.getLastIdentifierValue()) + 1);
		}

		HashMap<String, String> values = new HashMap<String, String>();

		values.put("behandelCode", object.getBehandelCode().getBehandelCode());
		values.put("patientId", String.valueOf(object.getPatient().getBsn()));
		values.put("fysiotherapeutId",
				String.valueOf(object.getFysiotherapeut().getBsn()));
		values.put("id", String.valueOf(object.getId()));

		return values;
	}

	@Override
	protected BehandelTraject createInstanceFromValues(
			HashMap<String, String> values) {
		if (values != null) {
			logger.debug("Create instance from values");
			try {
				PatientDAOInf patientDAO = new XMLDOMPatientDAO();
				ImmutablePatient patient = patientDAO.getByBSN(values
						.get("patientId"));

				logger.debug("Patient opgehaald : " + patient.getBsn());

				FysiotherapeutDAOInf fysiotherapeutDAO = new XMLDOMFysiotherapeutDAO();
				Fysiotherapeut fysiotherapeut = fysiotherapeutDAO.get(values
						.get("fysiotherapeutId"));

				logger.debug("Fysiotherapeut opgehaald: "
						+ fysiotherapeut.getBsn());

				BehandelCodeDAOInf behandelCodeDAO = new XMLDOMBehandelCodeDAO();
				BehandelCode behandelCode = behandelCodeDAO.get(values
						.get("behandelCode"));

				logger.debug("BehandelCode opgehaald: "
						+ behandelCode.getBehandelCode());

				BehandelTraject behandelTraject = new BehandelTraject(patient,
						fysiotherapeut, behandelCode);

				behandelTraject.setId(Integer.parseInt(values.get("id")));

				logger.debug("Behandeltraject aangemaakt");

				SessieDAOInf sessieDAO = new XMLDOMSessieDAO();
				ArrayList<Sessie> sessies = sessieDAO
						.getByBehandelTraject(Integer.parseInt(values.get("id")));

				for (Sessie sessie : sessies) {
					logger.debug("Voeg sessie toe aan behandeltraject: "
							+ sessie.getSessieNummer());
					behandelTraject.addSessie(sessie);
				}

				return behandelTraject;
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public ArrayList<BehandelTraject> getBehandelingen(FilterInf[] filters) {
		logger.debug("Haal alle behandelingen op uit xml");
		ArrayList<BehandelTraject> behandelingen = getAll();

		Iterator<BehandelTraject> behandelingIterator = behandelingen
				.iterator();

		while (behandelingIterator.hasNext()) {
			BehandelTraject behandelTraject = behandelingIterator.next();

			for (FilterInf filter : filters) {
				if (!filter.checkConstraints(behandelTraject)) {
					behandelingIterator.remove();

					// De overige filters hoeven niet meer gecontroleerd te
					// worden
					break;
				}
			}
		}

		logger.debug("Aantal behandelingen die voldoen aan de filters gevonden: "
				+ behandelingen.size());

		return behandelingen;
	}

	@Override
	public boolean delete(BehandelTraject object) {
		if (delete(object)) {
			SessieDAOInf sessieDAO = new XMLDOMSessieDAO();
			try {
				ArrayList<Sessie> sessies = sessieDAO
						.getByBehandelTraject(object.getId());

				for (Sessie sessie : sessies) {
					sessieDAO.delete(sessie);
				}

				return true;
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return false;
	}

	@Override
	public boolean heeftBehandelingen(FilterInf[] filters) {
		ArrayList<BehandelTraject> behandelingen = getBehandelingen(filters);

		for (BehandelTraject behandeling : behandelingen) {
			for (ImmutableSessie sessie : behandeling.getSessies()) {

				boolean passed = true;

				for (FilterInf filter : filters) {
					if (!filter.checkConstraints(sessie)) {
						passed = false;
					}
				}

				if (passed) {
					return true;
				}
			}
		}

		logger.debug("Fysiotherapeut heeft geen behandeling");
		return false;
	}

	@Override
	public boolean isMogelijk(ImmutableBehandelTraject behandelTraject,
			ImmutableSessie sessie) {
		ImmutableFysiotherapeut fysio = behandelTraject.getFysiotherapeut();

		logger.debug("Controleer of fysiotherapeut beschikbaar is");

		FilterInf[] filters = new FilterInf[2];

		FysiotherapeutFilter fysioFilter = new FysiotherapeutFilter(fysio);

		DatumFilter datumFilter = new DatumFilter(
				sessie.getBeginDatumTijd(), sessie.getEindDatumTijd(), true);

		filters[0] = fysioFilter;
		filters[1] = datumFilter;

		if (!heeftBehandelingen(filters)) {
			logger.debug("Fysiotherapeut is beschikbaar");
			return true;
		}
		
		logger.debug("Fysiotherapeut heeft al behandelingen op dit tijdstip");
		logger.debug("Fysiotherapeut is niet beschikbaar");
		
		return false;
	}

}
