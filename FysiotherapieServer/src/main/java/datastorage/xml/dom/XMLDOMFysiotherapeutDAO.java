/**
 * 
 */
package datastorage.xml.dom;

import interfaces.FysiotherapeutDAOInf;

import java.util.HashMap;

import org.apache.log4j.Logger;

import businessEntityDomain.Fysiotherapeut;

/**
 * @author robbie
 *
 */
public class XMLDOMFysiotherapeutDAO extends XMLDOMDAO<Fysiotherapeut>
		implements FysiotherapeutDAOInf {
	
	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(XMLDOMDAO.class);
	
	public XMLDOMFysiotherapeutDAO() {
		super(
				new XMLInformation(
						XMLInformation.elementsArrayToMap(new String[] {
								"voornaam", "achternaam", "geslacht", "adres",
								"postcode", "plaats", "telefoonnummer",
								"email", "rekeningnummer" }),
						"fysiotherapeuten", "fysiotherapeut", "bsn", "last_bsn"));
		logger.debug("XMLDOMFysiotherapeutDAO constructor");
	}
	
	@Override
	public boolean insert(Fysiotherapeut object) {
		return insert(object, false);
	}

	@Override
	protected HashMap<String, String> createValues(Fysiotherapeut object, boolean insert) {
		HashMap<String, String> values = new HashMap<String, String>();

		values.put("bsn", object.getBsn());
		values.put("voornaam", object.getVoornaam());
		values.put("achternaam", object.getAchternaam());
		values.put("geslacht", object.getGeslacht());
		values.put("adres", object.getAdres());
		values.put("postcode", object.getPostcode());
		values.put("plaats", object.getPlaats());
		values.put("telefoonnummer", object.getTelefoonnummer());
		values.put("email", object.getEmail());
		values.put("rekeningnummer", object.getRekeningnummer());

		return values;
	}

	@Override
	protected Fysiotherapeut createInstanceFromValues(
			HashMap<String, String> values) {
		if (values != null) {

			return new Fysiotherapeut(values.get("bsn"),
					values.get("voornaam"), values.get("achternaam"),
					values.get("geslacht"), values.get("adres"),
					values.get("postcode"), values.get("plaats"),
					values.get("telefoonnummer"), values.get("email"),
					values.get("rekeningnummer"));
		}

		return null;
	}

}
