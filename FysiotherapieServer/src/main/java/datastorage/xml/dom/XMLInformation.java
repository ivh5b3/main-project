/**
 * 
 */
package datastorage.xml.dom;

import java.util.LinkedHashMap;
import java.util.Properties;

import util.Settings;

import com.google.common.base.CaseFormat;

/**
 * @author robbie
 *
 */
public class XMLInformation {

	private LinkedHashMap<String, String> elements;
	private String rootName;
	private String recordName;
	private String identifierName;
	private String xmlFileName;
	private String xmlSchemaName;
	private String lastIdentifierElementName;
	private String pathToXMLFiles;

	public XMLInformation(LinkedHashMap<String, String> elements, String rootName,
			String recordName, String identifierName, String xmlFileName,
			String xmlSchemaName, String lastIdentifierElementName) {
		this.elements = elements;
		this.rootName = rootName;
		this.recordName = recordName;
		this.identifierName = identifierName;
		this.xmlFileName = xmlFileName;
		this.xmlSchemaName = xmlSchemaName;
		this.lastIdentifierElementName = lastIdentifierElementName;
		
		Properties properties = new Settings();
		this.pathToXMLFiles = properties.getProperty("path_to_xml");
	}

	public XMLInformation(LinkedHashMap<String, String> elements, String rootName,
			String recordName, String identifierName,
			String lastIdentifierElementName) {
		this(elements, rootName, recordName, identifierName, rootName
				+ ".xml", rootName + ".xsd", lastIdentifierElementName);
	}

	public XMLInformation(LinkedHashMap<String, String> elements, String rootName,
			String recordName) {
		this(elements, rootName, recordName, "id", "last_id");
	}

	public static LinkedHashMap<String, String> elementsArrayToMap(
			String[] elements) {
		LinkedHashMap<String, String> elementsMap = new LinkedHashMap<String, String>();

		for (String attribute : elements) {
			elementsMap.put(attribute, CaseFormat.LOWER_CAMEL.to(
					CaseFormat.LOWER_UNDERSCORE, attribute));
		}

		return elementsMap;
	}

	public String getIdentifierName() {
		return identifierName;
	}

	public String[] getElements() {
		return elements.keySet().toArray(new String[elements.size()]);
	}

	public String getXMLElementName(String element) {
		return elements.get(element);
	}

	public String getRootName() {
		return rootName;
	}

	public String getRecordName() {
		return recordName;
	}

	public String getXMLFileName() {
		return pathToXMLFiles + xmlFileName;
	}

	public String getXMLSchemaName() {
		return pathToXMLFiles + xmlSchemaName;
	}

	public String getLastIdentifierElementName() {
		return lastIdentifierElementName;
	}
	
	public void setPathToXMLFiles(String path) {
		this.pathToXMLFiles = path;
	}
}