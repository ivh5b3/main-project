/**
 * 
 */
package datastorage.xml.dom;

/**
 * @author robbie
 *
 */
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import datastorage.XMLStorable;

public abstract class XMLDOMDAO<T extends XMLStorable> {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(XMLDOMDAO.class);
	
	protected XMLDOMDocument domdocument;
	protected XMLDataControl dataControl;

	protected XMLDOMDAO(XMLInformation information) {
		this.domdocument = new XMLDOMDocument(information.getXMLFileName(),
				information.getXMLSchemaName());
		logger.debug(domdocument.getDocument());
		this.dataControl = new XMLDataControl(domdocument.getDocument(),
				information);
	}

	public boolean insert(T object) {
		return insert(object, true);
	}
	
	public boolean insert(T object, boolean insert) {

		logger.debug("Insert object");
		if (dataControl.insert(createValues(object, true), insert)) {
			logger.debug("Document is veranderd, schrijf het nu weg");
			
			domdocument.writeDocument();

			return true;
		}

		return false;
	}

	public boolean delete(T object) {
		if (dataControl.delete(createValues(object, false))) {
			domdocument.writeDocument();

			return true;
		}

		return false;
	}

	public boolean update(T object) {

		if (dataControl.update(createValues(object, false))) {
			domdocument.writeDocument();

			return true;
		}

		return false;
	}

	public T get(String id) {
		return createInstanceFromValues(dataControl.getByIdentifier(id));
	}

	public T get(int id) {
		return get(String.valueOf(id));
	}

	public ArrayList<T> getAll() {
		logger.debug("Haal alle objecten op uit xml");
		ArrayList<T> objects = new ArrayList<T>();

		ArrayList<HashMap<String, String>> arrayList = dataControl.getAll();
		logger.debug("Maak instancies van de values");
		for (HashMap<String, String> map : arrayList) {
			logger.debug("Maak instance van valueMap");
			objects.add(createInstanceFromValues(map));
		}
		
		logger.debug("Aantal objects gevonden: " + objects.size());

		return objects;
	}

	public ArrayList<T> getWhereElements(HashMap<String, String> wheres) {
		ArrayList<T> objects = new ArrayList<T>();

		ArrayList<HashMap<String, String>> arrayList = dataControl
				.getWhereElements(wheres);

		for (HashMap<String, String> map : arrayList) {
			objects.add(createInstanceFromValues(map));
		}

		return objects;
	}

	public ArrayList<T> getWhereAttributes(HashMap<String, String> attributes) {
		ArrayList<T> objects = new ArrayList<T>();

		ArrayList<HashMap<String, String>> arrayList = dataControl
				.getWhereElements(attributes);

		for (HashMap<String, String> map : arrayList) {
			objects.add(createInstanceFromValues(map));
		}

		return objects;
	}
	
	abstract protected HashMap<String, String> createValues(T object, boolean insert);

	abstract protected T createInstanceFromValues(HashMap<String, String> values);
}
