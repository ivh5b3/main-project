/**
 * 
 */
package rmi;

import interfaces.BehandelTrajectDAOInf;
import interfaces.FysiotherapieInf;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import util.Settings;
import businessEntityDomain.BehandelTraject;
import businessEntityDomain.DatumFilter;
import businessEntityDomain.FilterInf;
import businessEntityDomain.ImmutableSessie;
import businessEntityDomain.PatientFilter;
import businessEntityDomain.StatusFilter;
import datastorage.xml.dom.XMLDOMBehandelTrajectDAO;

/**
 * @author robbie
 *
 */
public class FysiotherapieApiImpl implements FysiotherapieInf {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(FysiotherapieApiImpl.class);

	@Override
	public Map<ImmutableSessie, String> getSessies(String bsn, Timestamp datum)
			throws RemoteException {
		Map<ImmutableSessie, String> immutableSessies = new HashMap<ImmutableSessie, String>();

		FilterInf[] filters = new FilterInf[3];

		Date date = new Date();
		filters[0] = new DatumFilter(datum, new Timestamp(date.getTime()));

		filters[1] = new StatusFilter("afgerond");

		filters[2] = new PatientFilter(bsn);

		BehandelTrajectDAOInf behandelTrajectDAO = new XMLDOMBehandelTrajectDAO();
		ArrayList<BehandelTraject> behandelingen = behandelTrajectDAO
				.getBehandelingen(filters);

		logger.debug("Aantal behandelingen gevonden voor api: "
				+ behandelingen.size());

		for (BehandelTraject behandeling : behandelingen) {
			String behandelCode = behandeling.getBehandelCode()
					.getBehandelCode();

			logger.debug("Aantal sessies gevonden voor behandeltraject: "
					+ behandeling.getSessies(filters).size());

			for (ImmutableSessie sessie : behandeling.getSessies()) {
				immutableSessies.put(sessie, behandelCode);
			}
		}

		return immutableSessies;
	}

	@Override
	public int getFysiotherapiePraktijkId() throws RemoteException {

		Properties properties = new Settings();

		return Integer.parseInt(properties.getProperty("praktijk_id"));
	}
}
